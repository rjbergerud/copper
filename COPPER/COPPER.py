# The Canadian Opportunities for Planning and Production of Electricity Resources (COPPER) model for Canada electricity system, national and provincial scale.
# Written by Reza Arjmand, Ph.D candidate at UVic (rezaarjmand@uvic.ca; rezaarjmand@yahoo.com). Linkedin: www.linkedin.com/in/rezaarjmand
# Please CITE this paper (Arjmand, R., & McPherson, M. (2022). Canada's electricity system transition under alternative policy scenarios. Energy Policy, April 2022)
# https://doi.org/10.1016/j.enpol.2022.112844.
# For more info: https://gitlab.com/McPherson/copper 
# Version 9.0

from pyomo.environ import *
import pandas as pd
import numpy as np
import os
import gc
import time
import sys
import toml
from datetime import datetime, timedelta

scenario = 'BAU_scenario'
os.chdir(scenario)

# Starting timer
start = time.time()

# Load config file
config_file= toml.load('config.toml')

#Carbon tax using average tax between 2025-2029 = 143
ctax= config_file['Carbon']['ctax']

#load_shedding_cost [$/MWh]
load_shedding_cost= config_file['Economics']['load_shedding_cost']

### planning reserve data
reserve_margin_df=pd.read_csv(r'./demand/reserve_margin.csv',header=None)
reserve_margin_df = reserve_margin_df.tail(-1)
reserve_margin_dict = {}
for index, row in reserve_margin_df.iterrows():
    reserve_margin_dict.update({row[0]:row[1]})

###percent of available hydro that can be upgraded to  store energy (retrofit to add pumped hydro storage)
pump_ret_limit= config_file['Hydro']['pump_ret_limit']
## discount rate expressed as a decimal
discount = config_file['Economics']['discount']
## inflation rate expressed as a decimal
inflation=config_file['Economics']['inflation']
## discount rate for year 2050 - 10x regular discount
discount_2050 = config_file['Economics']['discount_2050']
#downsampling or hierarchical clustering
downsampling= config_file['Simulation_Settings']['downsampling']
hierarchical= config_file['Simulation_Settings']['hierarchical']
# for test run
test=config_file['Simulation_Settings']['test']
# flag for hydro development
hydro_development=config_file['Hydro']['hydro_development']
# Minimal flow of hydro installations
hydro_minflow=config_file['Hydro']['hydro_minflow']
# flag for autarky - a scenario of limited trade between balancing areas
autarky= config_file['Regulations']['autarky']
# flag for continuous storage
storage_continous= config_file['Storage']['storage_continuous']
# flag for provincial emission limit
provincial_emission_limit= config_file['Carbon']['provincial_emission_limit']
emission_limit_ref_year= config_file['Carbon']['emission_limit_ref_year']
# flag for national emission limit
national_emission_limit= config_file['Carbon']['national_emission_limit']
# dictionary of periods (keys) and their associated emission limits (values)
nat_em_limit= config_file['Carbon']['nat_em_limit']
## local or national natural gas price
local_gas_price=config_file['Carbon']['local_gas_price']
## The OBPS flag is to be implemented (does nothing at the moment)
OBPS_on= config_file['Regulations']['OBPS_on'] # OBPS is always on
## GPS and CPO regulations
GPS= config_file['Regulations']['GPS']
CPO= config_file['Regulations']['CPO']
##constrained transmission expansion, constrains the tranmission expansion capacity to a coefficient of the current capacity
CTE_extant= config_file['Transmission']['CTE_extant']
## percent allowable increase in transmission beyond the original capacities (0 indicates a grid fixed to its input transmission capacities; 1 implies an allowable increase of 100% of the input transmission capacities)
CTE_coef= config_file['Transmission']['CTE_coef']
## flag for a custom transmission capacity scenario - user must include the trans_expansion_limits.csv file to submit input for transmission capacity constraints
CTE_custom=config_file['Transmission']['CTE_custom']
## input for custom transmission capacity constraints
transmission_data = pd.read_csv(r'transmission.csv')
if CTE_custom:
    trans_expansion_limits=dict(transmission_data[['Transmission Line', 'Expansion_limits']].values)
## Thermal unit phase out, type:year
thermal_phase_out= config_file['Phase_out']['thermal_phase_out']
phase_out_type_year= config_file['Phase_out']['phase_out_type_year']
ph_out_t=list(phase_out_type_year.keys())
## just consider small hydro projects, under 100 MW
just_small_hydro= config_file['Hydro']['just_small_hydro']
## flag for non-emitting limit
non_emitting_limit= config_file['Electrical_grid']['non_emitting_limit']
## limit electrcity generation emission
nonemitting_limit= config_file['Electrical_grid']['nonemitting_limit']
## technology evolutiion level
technology_evolution_on= config_file['Economics']['technology_evolution_on']
## limited capacity addition of a generation type
limited_new_thermal_gen= config_file['Thermal']['limited_new_thermal_gen']
## renewable portfolio standards - flags for minimum and maximum capacities for wind and solar generation
renewable_portfolio_standards= config_file['Constraints']['renewable_portfolio_standards']
## input files for minimum and maximum capacities for wind and solar generation
renewable_portfolio_max = pd.read_csv(r'renewable_portfolio_max.csv', index_col=0)
renewable_portfolio_min = pd.read_csv(r'renewable_portfolio_min.csv', index_col=0)

wind_ons_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "wind_ons"').drop(['Technology'],axis=1)
wind_ons_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "wind_ons"').drop(['Technology'],axis=1)
wind_ofs_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "wind_ofs"').drop(['Technology'],axis=1)
wind_ofs_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "wind_ofs"').drop(['Technology'],axis=1)
solar_max_ap_limit = renewable_portfolio_max.copy().query('Technology == "solar"').drop(['Technology'],axis=1)
solar_min_ap_limit = renewable_portfolio_min.copy().query('Technology == "solar"').drop(['Technology'],axis=1)
### flag for nuclear capacity constraints by balancing area
max_nuclear_aba = config_file['Constraints']['max_nuclear_aba']
### flag for SMR capacity constraints by balancing area
max_smr_aba = config_file['Constraints']['max_smr_aba']
### flag for biomass capacity constraints by balancing area
max_bio_aba = config_file['Constraints']['max_bio_aba']
### flag for calculating the reserve margin values at the end
reserve_calculation = config_file['Electrical_grid']['reserve_calculation']

###No CCS except for AB and SK
no_CCS_aba= config_file['Carbon']['no_CCS_aba']
###CCS technologies
CCS_tech= config_file['Carbon']['CCS_tech']
###CCS technologies
cg_tech= config_file['Carbon']['cg_tech']


### CER regulation block ###
## flag for Clean Electricity Regulations (CER)
flag_cer = config_file['Regulations']['CER']['flag_cer']
### global parameter for maximum number of operating hours for backup generators
max_op_backup_hours = config_file['Regulations']['CER']['max_op_backup_hours']
### global parameter for emissions intensity threshold -tCO2e/MWh -JGM
max_em_intensity = config_file['Regulations']['CER']['max_em_intensity']
### global parameter for maximum carbon emissions for backup -tCO2e
max_em_backup = config_file['Regulations']['CER']['max_em_backup'] #150000 tCO2e/unit * 1 unit/148 MW = 1013.50 tCO2e/MW
## backup generation technologies
backup_generators = config_file['Regulations']['CER']['backup_generators']
## retired generation technologies
retired_generators = config_file['Regulations']['CER']['retired_generators']
## pre2025 fossil emitting thermal technologies
pre2025_generators = config_file['Regulations']['CER']['pre2025_generators']

## flag for Investment Tax Credit (ITC)
flag_itc = config_file['Economics']['flag_itc']
## ITC factor for 15% discount on cap cost
factor_itc = config_file['Economics']['factor_itc']

#Initializing all sets
### All regions
ap= config_file['Simulation_Settings']['ap'] #all provinces
aba=config_file['Simulation_Settings']['aba'] #all possible balancing areas
###auxiliary, just for creatin input files####
aba1= config_file['Simulation_Settings']['aba1']
## model sub-periods which act as stepping points in the simulation to track the trajectory of the network configuration
pds= config_file['Simulation_Settings']['pds'] #Also common to simulate with 5-year sub-periods - ['2025','2030','2035','2040','2045','2050']
season= config_file['Simulation_Settings']['season']

regions_per_province = dict()
for AP in ap:
    regions_per_province.update({AP:[ABA for ABA in aba if AP in ABA]})

provinces_per_region = dict()
for ABA in aba:
    provinces_per_region.update({ABA:[AP for AP in ap if AP in ABA]})

capacity_val=pd.read_csv(r'wind_solar_capacity_value.csv',header=None)
header_A=list(capacity_val.iloc[0,:])
header_B=list(capacity_val.iloc[1,:])
del(header_A[0])
del(header_B[0])
ind=list(capacity_val.iloc[:,0])
del(ind[0])
del(ind[0])
capacity_value = pd.DataFrame(np.array(capacity_val.loc[2:12,1:5]), columns = pd.MultiIndex.from_tuples(zip(header_A,header_B)), index=ind)
capacity_val_inds = config_file['Simulation_Settings']['capacity_val_inds']
solar_wind_capacity = {TECH+'.'+SEASON+'.'+ABA:float(capacity_value.values[ap.index(provinces_per_region[ABA][0])][capacity_val_inds.index(SEASON+'.'+TECH)]) for ABA in aba for SEASON in season for TECH in ['wind','solar'] }

tech_evolution=pd.read_csv(r'technology_evolution.csv',header=0,index_col=0)
technology_evolution=dict()
for PD in pds:
    for GT in ['wind_ons','wind_ofs','solar','storage']:
        if technology_evolution_on['base']:
            technology_evolution[GT+'.'+PD]=tech_evolution[PD][GT+'.'+'base']   ### canada energy future 2020 base scenario
        elif technology_evolution_on['evolving']:
            technology_evolution[GT+'.'+PD]=tech_evolution[PD][GT+'.'+'evolving']
        else:
            technology_evolution[GT+'.'+PD]=1

reserve_margin_tech = pd.read_csv(r'capacity_value.csv',header=0)
reserve_margin_tech_type = list(reserve_margin_tech.iloc[:]['Type'])
reserve_margin_tech_raw_dict = {AP:reserve_margin_tech.iloc[:][AP] for AP in ap}

reserve_margin_tech_dict = {TECH+'.'+AP:reserve_margin_tech_raw_dict[AP][reserve_margin_tech_type.index(TECH)] for TECH in reserve_margin_tech_type for AP in ap}

# Definition of the qualifying capacity for winter and summer (wind and solar)
qualifing_capacity_summer = reserve_margin_tech_dict.copy()
qualifing_capacity_winter = reserve_margin_tech_dict.copy()

for cap_renewal in [key for key in solar_wind_capacity.keys() if 'summer' in key]:
    qualifing_capacity_summer.update({cap_renewal:solar_wind_capacity[cap_renewal]})

for cap_renewal in [key for key in solar_wind_capacity.keys() if 'winter' in key]:
    qualifing_capacity_winter.update({cap_renewal:solar_wind_capacity[cap_renewal]})

#### gas PHP installed capacity limit
LB_PHP_installed_limit=dict()
for ABA in aba:
    LB_PHP_installed_limit[ABA]=0

##we can skip running some days using sample_rate, for example if sample_rate=3 it will run days 1,4,7,10,...
# number of days that we want to run
if downsampling:
   sample_rate= config_file['Simulation_Settings']['sample_rate']
   rundaynum= config_file['Simulation_Settings']['run_day_number']
   rundays=list(range(1,rundaynum+1,sample_rate))
   cap_cost_alter=(365/len(rundays))
### uses hierarchical clustering results  
elif hierarchical:
     if test:
         run_days = config_file['Simulation_Settings']['run_days_test']
     else:
         run_days = config_file['Simulation_Settings']['run_days']
     rundays=run_days
     rundays=[int(RD) for RD in rundays]
     cap_cost_alter=(365/len(rundays))
else:
    rundays=list(range(1,366))
    cap_cost_alter=1

##### Generation fleets data

gendata = pd.read_csv (r'generation_type_data.csv',header=0 )

#####Storage dataol(x) -
# storage technology
st= config_file['Storage']['st']
storage_hours= config_file['Storage']['storage_hours']
#some data is pulled from input sheet and put into a dictionary by type
storage_data = {
    s_type: {
        'capitalcost': gendata[gendata['Type'] == s_type]['capitalcost'].iloc[0],
        'fixed_o_m': gendata[gendata['Type'] == s_type]['fixed_o_m'].iloc[0],
        'efficiency': gendata[gendata['Type'] == s_type]['efficiency'].iloc[0]
    }
    for s_type in st}
#capital and efficiency are isolated from the above dictionary and put into more specific dictionaries
storage_capital = {s_type: storage_data[s_type]['capitalcost'] for s_type in st}
storage_efficiency = {s_type: storage_data[s_type]['efficiency'] for s_type in st}
#storage cost is calculated considering technology evolution
storage_cost=dict()
for PD in pds:
    for ST in st:
        if ST+'.'+PD in technology_evolution:
            storage_cost[ST+'.'+PD]=storage_capital[ST]*technology_evolution[ST+'.'+PD]
        else:
            storage_cost[ST+'.'+PD]=storage_capital[ST]
#fixed operation and maintenance cost of storage technologies is put into dictionaries for each year.
ph_fix_o_m = storage_data['storage_PH']['fixed_o_m']
li_fix_o_m = storage_data['storage_LI']['fixed_o_m']
store_fix_o_m={'storage_PH.2030':ph_fix_o_m,'storage_PH.2040':ph_fix_o_m,'storage_PH.2050':ph_fix_o_m,'storage_LI.2030':li_fix_o_m,'storage_LI.2040':li_fix_o_m,'storage_LI.2050':li_fix_o_m,
               'storage_PH.2025':ph_fix_o_m,'storage_PH.2035':ph_fix_o_m,'storage_PH.2045':ph_fix_o_m,'storage_LI.2025':li_fix_o_m,'storage_LI.2035':li_fix_o_m,'storage_LI.2045':li_fix_o_m}

runhours=rundays[-1]*24
foryear= config_file['Simulation_Settings']['forecast_year']
refyear= config_file['Simulation_Settings']['reference_year']

## converts GJ to MWh
GJtoMWh= 3.6
autonomy_pct= config_file['Regulations']['autonomy_pct']
#share carbon reduced
carbon_reduction= config_file['Carbon']['carbon_reduction']
#reference case carbon emissions in electricity sector in 2005 in Mt by province (source: https://www.cer-rec.gc.ca/en/data-analysis/energy-markets/provincial-territorial-energy-profiles/provincial-territorial-energy-profiles-explore.html)
carbon_2005_ref={"British Columbia" :1.04, "Alberta" :48.83,  "Saskatchewan" :14.82, "Manitoba" :.36, "Ontario" :33.9, "Quebec" :0.65, "New Brunswick" :7.8, "Newfoundland and Labrador" :0.82,  "Nova Scotia" :10.77,"Prince Edward Island" :0}

#reference case carbon emissions in electricity sector in 2017 in Mt by province (source: https://www.cer-rec.gc.ca/en/data-analysis/energy-markets/provincial-territorial-energy-profiles/provincial-territorial-energy-profiles-explore.html)
carbon_2017_ref={"British Columbia" :0.15, "Alberta" :44.33,  "Saskatchewan" :15.53, "Manitoba" :.07, "Ontario" :1.99, "Quebec" :0.26, "New Brunswick" :3.65, "Newfoundland and Labrador" :1.53,  "Nova Scotia" :6.5,"Prince Edward Island" :0.01}
#maximum carbon emissions in the target year in Mt
carbon_limit=dict()
if emission_limit_ref_year==2017:
    for AP in ap:
        carbon_limit[AP]=carbon_2017_ref[AP]*(1-carbon_reduction)
       
elif emission_limit_ref_year==2005:
    for AP in ap:
        carbon_limit[AP]=carbon_2005_ref[AP]*(1-carbon_reduction)

h=list(range(1,8761))

gridcell_data = pd.read_csv(os.path.join(os.getcwd(), '..', 'static/gridcells.csv'))

map_gl_to_ba=dict(gridcell_data[['grid_cell', 'ba']].values) #map grid locations to balancing areas
map_gl_to_pr=dict(gridcell_data[['grid_cell', 'pr']].values) #map grid locations to provinces

allplants=list(gendata.iloc[:]['Type'])
print(allplants) #JGM16

tplants=list()
isthermal=list(gendata.iloc[:]['Is thermal?'])
cc=0
for i in isthermal:
    if i:
        tplants.append(allplants[cc])
    cc+=1
print('at start of tplants!')
print(tplants)

ITCplants=list()
if flag_itc:
    isITCsupport=list(gendata.iloc[:]['ITC support?'])
    cc=0
    for i in isITCsupport:
        if i:
            ITCplants.append(allplants[cc])
        cc+=1
    print('at start of ITCplants!')
    print(ITCplants)
#Fossil-emitting thermal plants with emission intensities over the limit
ftplants=list()
fuel_co2=list(gendata.iloc[:]['fuel_co2'])
fuel_co2_obps_2025=list(gendata.iloc[:]['fuel_co2_obps_2025'])
fuel_co2_obps_2030=list(gendata.iloc[:]['fuel_co2_obps_2030'])

tplant_fuel_co2_dict_cer = {tplant:fuel_co2[allplants.index(tplant)] for tplant in allplants}

tplant_fuel_co2_dict_obps_2025 = {tplant:fuel_co2_obps_2025[allplants.index(tplant)] for tplant in allplants}

tplant_fuel_co2_dict_obps_2030 = {tplant:fuel_co2_obps_2030[allplants.index(tplant)] for tplant in allplants}

cc=0
for i in fuel_co2:
    if (i > max_em_intensity) and (allplants[cc] not in backup_generators) and (allplants[cc] not in pre2025_generators):
        ftplants.append(allplants[cc])
    cc+=1

## set of non-emitting generation technologies
non_emitting_tplants=list()
non_emitting=list(gendata.iloc[:]['non-emitting thermal?'])
cc=0
for i in non_emitting:
    if i:
        non_emitting_tplants.append(allplants[cc])
    cc+=1

## for linkage feedback
min_installed_LB_PHP= config_file['Storage']['min_installed_LB_PHP']
## new thermal unit restriction, types
new_thermal_limit= config_file['Thermal']['new_thermal_limit']
## list of thermal generation technologies that are restricted for new development
old_limited_tplants= config_file['Thermal']['old_limited_tplants']
limited_tplants = [tplant for tplant in tplants for category in old_limited_tplants if category in tplant]

## Reading thermal units specifications
max_cap_fact=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['max_cap_fact'])))#{'gasCC':0.8, 'gasSC': .3, 'nuclear': 0.95, 'coal': 0.9, 'diesel': 0.95, 'biomass': 0.9} #annual maximum capacity factor
min_cap_fact=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['min_cap_fact'])))#{'gasCC': 0.2, 'gasSC': .02, 'nuclear': 0.75,  'coal': 0.5, 'diesel': 0.05, 'biomass': 0.2} #annual minimum capacity factor
ramp_rate_percent=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['ramp_rate_percent'])))#{'gasCC': 0.1, 'gasSC':0.1 , 'nuclear': 0.05,  'coal': 0.05, 'diesel': 0.1, 'biomass': 0.05} #ramp rate in percent of capacity per hour
efficiency=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['efficiency'])))#{'gasCC': 0.509, 'gasSC': 0.28, 'nuclear': 0.327, 'coal': 0.39, 'diesel': 0.39, 'biomass': 0.39}

### Reading cost data
fixed_o_m=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['fixed_o_m'])))#(fixedom.values)
 
variable_o_m=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['variable_o_m'])))#dict(variableom.values)
fuelprice=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['fuelprice'])))#dict(fuel_price.values)

capitalcost1=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['capitalcost'])))#dict(capital_cost.values)
if technology_evolution_on['evolving']:
    capitalcost1['nuclear_SMR']=629574 # TODO: Move this to gen_type_data?
capitalcost=dict()
for ABA in aba:
    for G in capitalcost1:
        capitalcost[ABA+'.'+G]=capitalcost1[G]


if GPS:
    for ABA in aba:
        capitalcost[ABA+'.gasSC']=1000000000
        capitalcost[ABA+'.coal']=1000000000

   
#### Transmission sytem costs
trans_o_m= config_file['Transmission']['trans_o_m']
transcost= config_file['Transmission']['transcost']
intra_ba_transcost= config_file['Transmission']['intra_ba_transcost']

gridlocations= list(range(1,2984))
gl=[str(GL) for GL in gridlocations]

distance_to_grid=dict(gridcell_data[['grid_cell', 'distance_to_grid']].values)
windonscost=dict()
windofscost=dict()
solarcost=dict()
for PD in pds:
    for GL in gl:
        windonscost[PD+'.'+GL]=capitalcost[map_gl_to_ba[int(GL)]+'.wind_ons']*technology_evolution['wind_ons.'+PD]+distance_to_grid[int(GL)]*intra_ba_transcost
        windofscost[PD+'.'+GL]=capitalcost[map_gl_to_ba[int(GL)]+'.wind_ofs']*technology_evolution['wind_ofs.'+PD]+distance_to_grid[int(GL)]*intra_ba_transcost
        solarcost[PD+'.'+GL] = capitalcost[map_gl_to_ba[int(GL)]+'.solar']*technology_evolution['solar.'+PD] + distance_to_grid[int(GL)]*intra_ba_transcost

#thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['fuel_co2'])))
#thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide_obps_2025=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['fuel_co2'])))
#thermal plant co2 emissions in tonne per MWh of electricity generated
carbondioxide_obps_2030=dict(zip(list(gendata.iloc[:]['Type']),list(gendata.iloc[:]['fuel_co2'])))

#### Adding carbon price to the fuelcost, fuelcost=fuelprice + carbonprice ($/MWh)
## ref CERI study 168 table 3.11 from Statistics Canada (CANSIM Table 129-0003 Sales of natural gas, annual)
if local_gas_price:
    gasprice= config_file['Thermal']['gasprice']
else:
    gasprice=dict()
    for ABA in aba:
        gasprice[ABA]=fuelprice['gasCC']

# Calculation of fuel cost in function of OBPS and CER regulations
fuelcost=dict()
for PD in pds:
    for TP in tplants:
        for ABA in aba:
            ### This first part is computed for the actual fuel cost, it happens regardless
            if TP=='gasCC_pre2025' or TP=='gasCC_ccs_pre2025' or TP=='gasCC_backup_pre2025' or TP=='gasCC_backup_post2025' or TP=='gasSC_pre2025' or TP=='gasSC_ccs_pre2025' or TP=='gasSC_backup_pre2025' or TP=='gasSC_backup_post2025':
                fuelcost[PD+'.'+TP+'.'+ABA] = (gasprice[ABA]/efficiency[TP])*GJtoMWh
            else:
                fuelcost[PD+'.'+TP+'.'+ABA] = (fuelprice[TP]/efficiency[TP])*GJtoMWh
            ###This next part adds in the carbon cost to the fuel cost, different if CER flag is activated
            if PD == '2025': ## In 2025 the OBPS is in effect no matter what, charge carbon tax on residual emissions
                fuelcost[PD+'.'+TP+'.'+ABA]+=carbondioxide_obps_2025[TP]*ctax[PD]
            elif PD == '2030': ## In 2030 the OBPS is in effect no matter what, charge carbon tax on residual emissions
                fuelcost[PD+'.'+TP+'.'+ABA]+=carbondioxide_obps_2030[TP]*ctax[PD]
            elif flag_cer: ## If CER is on, then full carbon tax is applied 2035-2050
                fuelcost[PD+'.'+TP+'.'+ABA]+=carbondioxide[TP]*ctax[PD]
            else: # If CER is false, then OBPS carbon tax is applied 2035-2050 with the OBPS 2030 rate
                fuelcost[PD+'.'+TP+'.'+ABA]+=carbondioxide_obps_2030[TP]*ctax[PD]

max_development = pd.read_csv(r'max_capacity_limits.csv')
### Reading input file for nuclear development (By balancing area)
max_nuclear = dict(max_development[['ABA', 'MAX_nuclear']].values)

### Reading input file for SMR development (By balancing area)
max_smr = dict(max_development[['ABA', 'MAX_SMR']].values)

### Reading input file for biomass development (By balancing area)
max_bio = dict(max_development[['ABA', 'MAX_bio']].values)

### Reading transmission expansion routes  
transmap=list(transmission_data['Transmission Line'].values)

distance = dict(transmission_data[['Transmission Line', 'Distance']].values)

### Reading extant wind and solar data
extantwindsolar = pd.read_csv(r'extant_wind_solar.csv')

extant_wind_solar=list()
for PD in pds:
    extant_wind_solar.append(dict(extantwindsolar[['location', f'{PD}']].values))

extantwindsolar = extantwindsolar.reset_index(drop=True)
extantwindsolar[["gl","type"]] = extantwindsolar["location"].str.split(".", expand=True)
extantwindsolar["gl"] = extantwindsolar["gl"].astype(int)
extantwindsolar["aba"] = extantwindsolar["gl"].apply(map_gl_to_ba.get)
existing_capacity = extantwindsolar.groupby(["aba","type"]).sum(numeric_only=True)
existing_capacity.drop("gl", axis=1, inplace=True)

### Reading extant generation capacity data
extantcapacity = pd.read_csv(r'extant_capacity.csv',header=0)
extant_capacity=dict()
for PD in pds:
    label_ABA=list(extantcapacity.iloc[:]['ABA'])
    label_ABA=[PD+'.'+LL for LL in label_ABA]
    excap=dict(zip(label_ABA,list(extantcapacity.iloc[:][PD])))
    extant_capacity.update(excap)

periods_capacity_diffrance = extantcapacity [:]

for index, row in periods_capacity_diffrance.iterrows():
    for PD_index in reversed(range(7)):
        if PD_index != 0 and PD_index != 1:
            Capicity_table_entry = periods_capacity_diffrance.iat[index, PD_index]
            #print("Capicity_diffrance: ",Capicity_table_entry)
            periods_capacity_diffrance.iat[index, PD_index] = periods_capacity_diffrance.iat[index, PD_index-1] - periods_capacity_diffrance.iat[index, PD_index]
    periods_capacity_diffrance.iat[index, 1] = 0

periods_capacity_diffrance_dict=dict()
for PD in periods_capacity_diffrance.columns [1:]:
    label_ABA=list(periods_capacity_diffrance.iloc[:]['ABA'])
    label_ABA=[PD+'.'+LL for LL in label_ABA]
    excap=dict(zip(label_ABA,list(periods_capacity_diffrance.iloc[:][PD])))
    periods_capacity_diffrance_dict.update(excap)

### Reading extant tranmission lines capacity data
extanttrans = transmission_data.copy()
extanttrans = extanttrans.drop(['Distance', 'Expansion_limits'], axis=1)
extant_transmission=list()
for PD in pds:
    extant_transmission.append(dict(zip(list(extanttrans.iloc[:]['Transmission Line']),list(extanttrans.iloc[:][PD]))))
del extanttrans
print('Extant transmission')
print(extant_transmission)

##### Reading hydro units capacity factor data
hydrocf = pd.read_csv(os.path.join(os.getcwd(), '..', 'static/hydro_cf.csv'))
hydro_cf=dict(hydrocf.values)
del hydrocf

## ### Reading demand related data
demand_growth = pd.read_csv(r'./demand/annual_growth.csv',header=0,index_col=0)

##### Reading population data to disaggregate demand data
population = dict(gridcell_data[['grid_cell', 'population']].values)
population = {k: v for k, v in population.items() if pd.Series(v).notna().all()}

demand_us = dict(pd.read_csv(r'./demand/us_demand.csv',header=None).values)

##### Converting hours to days/months
start_date = datetime(refyear, 1, 1, 0, 0, 0)
year_hours = [start_date]
for i in range(1, 8760):
    start_date += timedelta(seconds=3600)
    year_hours.append(start_date)
year_hours = pd.DataFrame({'dates': year_hours})
year_hours['hour'] = year_hours.index+1
year_hours['day'] = (year_hours['hour'] - 1) // 24 + 1
year_hours['month'] = year_hours['dates'].dt.month

map_hd= dict(year_hours[['hour', 'day']].values)
map_hm=dict(year_hours[['hour', 'month']].values)


surface_area_solar = dict(gridcell_data[['grid_cell', 'surface_area_ons']].values)
surface_area_wind_ons = dict(gridcell_data[['grid_cell', 'surface_area_ons']].values)
surface_area_wind_ofs = dict(gridcell_data[['grid_cell', 'surface_area_ofs']].values)
## according to the sample_rate these lines remove the days that we don't want to run

demand_all = dict()
## Calculating seasonal peak demand
#national_demand = np.zeros((len(h),len(pds)))
peak_demand = dict()
peak_days = dict()
province_demand = dict()
for AP in ap:
    province_demand[AP] = np.zeros((len(h),len(pds)))

demand_piv = pd.read_csv(r"./demand/demand.csv", index_col=0) # Pivoted demand 

time_idx = pd.MultiIndex.from_product([pds, h], names=["period","hour"])
all_demand_df = pd.DataFrame(index=time_idx, columns=ap)

last_year = refyear


for PD in pds:
    for AP in ap:
        if PD == pds[0]:
            all_demand_df.loc[PD,AP]= (demand_piv[AP] * (1+demand_growth[PD][AP])**(int(PD)-last_year)).values
        else:
            all_demand_df.loc[PD,AP]= (all_demand_df.loc[str(last_year),AP] * (1+demand_growth[PD][AP])**(int(PD)-last_year)).values

        province_demand[AP][:,pds.index(PD)] = all_demand_df.loc[PD,AP]

        peak_demand[PD+'.'+'winter'+'.'+AP]= max(max(province_demand[AP][:2160,pds.index(PD)]),max(province_demand[AP][6480:8760,pds.index(PD)]))  
        peak_demand[PD+'.'+'summer'+'.'+AP]= max(province_demand[AP][2160:6480,pds.index(PD)])
        if (AP == 'New Brunswick'):
            print('AP = ')
            print(AP)
            print('Winter Peak = ')
            print(peak_demand[PD+'.'+'winter'+'.'+AP])
            print('Summer Peak = ')
            print(peak_demand[PD+'.'+'summer'+'.'+AP])
    last_year = int(PD)

del h[runhours:]
hours=len(h)

nummonths=map_hm[rundays[-1]*24]
m=list(range(1,nummonths+1))
       
d=rundays.copy()
h3=h.copy()
for H in h3:
    if map_hd[H] not in rundays:
        h.remove(H)

del h3
hours=len(h)
h2=h.copy()
del h2[hours-1]

start_slow_p = time.perf_counter()
## Reading hourly wind and solar CF data in each grid cell only for hours in rundays
def get_windcf(h, gl, rundays):
    windcf_df=pd.read_csv(os.path.join(os.getcwd(), '..', 'static/windcf.csv'),index_col="h", engine="pyarrow")

    meanwincf = windcf_df.mean().mean()
    # Filtering wincf data to include only the hours in rundays
    windcf_df = windcf_df.query("h == @h")
    meanwincf_reduced=windcf_df.mean().mean()
    
    windcf_df.columns = windcf_df.columns.astype(int)
    return windcf_df, meanwincf, meanwincf_reduced

[windcf_df, meanwincf, meanwincf_reduced] = get_windcf(h, gl, rundays)


def get_solarcf(h, gl, rundays):
    solarcf_df= pd.read_csv(os.path.join(os.getcwd(), '..', 'static/solarcf.csv'), index_col="h",engine="pyarrow")

    meansolarcf=solarcf_df.mean().mean()
    # Filtering solarcf data to only include the hours in rundays
    solarcf_df = solarcf_df.query("h == @h")
    meansolarcf_reduced=solarcf_df.mean().mean()
    solarcf_df.columns = solarcf_df.columns.astype(int)
    return solarcf_df, meansolarcf, meansolarcf_reduced
[solarcf_df, meansolarcf, meansolarcf_reduced] = get_solarcf(h, gl, rundays)
end_slow_p = time.perf_counter()

print("generating solar & wind cf's:", end_slow_p-start_slow_p)
    
##calculate the difference between hours in a row
time_diff=dict()
for I in list(range(len(h)-1)):
    time_diff[h[I]]=h[I+1]-h[I]

gl2=gl.copy()
for GL in gl2:
    if map_gl_to_pr[int(GL)] not in ap:
        gl.remove(GL)
       
###maximum wind and solar capacity that can be installed in each grid cell in MW per square km
maxwindonsperkmsq= config_file['Constraints']['maxwind_ons_perkmsq'] #2
maxwindofsperkmsq= config_file['Constraints']['maxwind_ofs_perkmsq'] #2 -I feel like this number should be changed in config for offshore... -JGM
maxsolarperkmsq= config_file['Constraints']['maxsolarperkmsq'] #31.28
maxwindons=dict()
maxwindofs=dict()
maxsolar=dict()
for GL in gl:
    maxwindons[GL]=surface_area_wind_ons[int(GL)]*maxwindonsperkmsq
    maxwindofs[GL]=surface_area_wind_ofs[int(GL)]*maxwindofsperkmsq
    maxsolar[GL]=surface_area_solar[int(GL)]*maxsolarperkmsq
## Extant PHS data
ba_storage_capacity=dict()
for ABA in aba:
    for ST in st:
        ba_storage_capacity[ABA+'.'+ST]=0
ba_storage_capacity['Ontario.a'+'.'+'storage_PH']=174

#Calculating transmission loss coefficients
translossfixed= config_file['Transmission']['translossfixed'] #0.02
translossperkm= config_file['Transmission']['translossperkm'] #0.00003
transloss=dict()
for ABA in aba:
    for ABBA in aba:
        if ABA+'.'+ABBA in distance:
            transloss[ABA+'.'+ABBA]=distance[ABA+'.'+ABBA]*translossperkm+translossfixed
## disaggregating demand based on population
populationaba=dict()
for ABA in aba:
    populationaba[ABA]=0
for ABA in aba:
    for GL in population:
        if map_gl_to_ba[int(GL)]==ABA:
            populationaba[ABA]=populationaba[ABA]+population[GL]
populationap=dict()
demand=dict()
demand_df = pd.DataFrame(index=pd.MultiIndex.from_product([pds,h]), columns=aba)

for PD in pds:
    for AP in ap:
        populationap[AP]=sum(populationaba[ABA] for ABA in aba if AP in ABA)
    for ABA in aba:
        pvba=ABA.replace('.a','')
        pvba=pvba.replace('.b','')
        demand_df.loc[PD,ABA] = (all_demand_df.query("hour in @h").loc[PD,pvba]*(populationaba[ABA]/populationap[pvba])).values

## Creating extant generation dict
extant_thermal=dict()
for AP in ap:
    for ABA in aba1:
        for TP in tplants:
            for PD in pds:
                extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=0
                if PD+'.'+AP+'.'+ABA+'.'+TP in extant_capacity:
                    extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=extant_capacity[PD+'.'+AP+'.'+ABA+'.'+TP]
                if CPO and TP=='coal' and int(PD)>=2030:
                    extant_thermal[PD+'.'+AP+'.'+ABA+'.'+TP]=0

hydro_capacity=dict()


# calculate max renewable generation

start_vectorized = time.perf_counter()

extant_solar_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds,h]), columns=aba).fillna(0)
extant_wind_ofs_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds,h]), columns=aba).fillna(0) 
extant_wind_ons_gen = pd.DataFrame(index=pd.MultiIndex.from_product([pds,h]), columns=aba).fillna(0)


for PD in pds:
    for ABA in aba:
        aba_gl_solar_caps = extantwindsolar.query(f"type=='solar' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = solarcf_df.loc[:,aba_gl_solar_caps.index] @ aba_gl_solar_caps
        extant_solar_gen.loc[PD,ABA] = max_gen.values
        
        aba_gl_windons_caps = extantwindsolar.query(f"type=='wind_ons' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = windcf_df.loc[:,aba_gl_windons_caps.index] @ aba_gl_windons_caps
        extant_wind_ons_gen.loc[PD,ABA] = max_gen.values

        # this is currently not existing, i.e. no "wind_ofs" in extant_wind.csv
        aba_gl_windofs_caps = extantwindsolar.query(f"type=='wind_ofs' and aba=='{ABA}'").set_index("gl")[PD]
        max_gen = windcf_df.loc[:,aba_gl_windofs_caps.index] @ aba_gl_windofs_caps
        extant_wind_ofs_gen.loc[PD,ABA] = max_gen.values
        pass

end_vectorized = time.perf_counter()
print("generating solar & wind output power using the new method took:", end_vectorized-start_vectorized)

## calculating hydro CF for different type of hydro power plant
start_slow_loop = time.perf_counter()
ror_hydroout=dict()
day_hydroout=dict()
month_hydroout=dict()
day_hydro_historic=dict()
month_hydro_historic=dict()
ror_hydro_capacity=dict()
day_hydro_capacity=dict()
month_hydro_capacity=dict()
day_minflow=dict()
month_minflow=dict()
for PD in pds:
    for AP in ap:
       for ABA in aba1:
           ror_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
           day_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
           month_hydro_capacity[PD+'.'+AP+'.'+ABA] = 0
           if PD+'.'+AP+'.'+ABA+'.'+'hydro_run' in extant_capacity:
               ror_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_run']
               day_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_daily']
               month_hydro_capacity[PD+'.'+AP+'.'+ABA] = extant_capacity[PD+'.'+AP+'.'+ABA+'.'+'hydro_monthly']
           day_minflow[PD+'.'+AP+'.'+ABA]=day_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_minflow
           month_minflow[PD+'.'+AP+'.'+ABA]=month_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_minflow
           for D in d:
               day_hydro_historic[PD+'.'+str(D)+'.'+AP+'.'+ABA]=0
               
           for M in m:
               month_hydro_historic[PD+'.'+str(M)+'.'+AP+'.'+ABA]=0
           for H in h:

             if AP+'.'+str(H) in hydro_cf:
                  ror_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA]=ror_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
                  day_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] = day_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
                  month_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] = month_hydro_capacity[PD+'.'+AP+'.'+ABA]*hydro_cf[AP+'.'+str(H)]
             else:
                  ror_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
                  day_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
                  month_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA] =0
               
           
             day_hydro_historic[PD+'.'+str(map_hd[H])+'.'+AP+'.'+ABA]=day_hydro_historic[PD+'.'+str(map_hd[H])+'.'+AP+'.'+ABA]+day_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA]
             month_hydro_historic[PD+'.'+str(map_hm[H])+'.'+AP+'.'+ABA]=month_hydro_historic[PD+'.'+str(map_hm[H])+'.'+AP+'.'+ABA]+month_hydroout[PD+'.'+str(H)+'.'+AP+'.'+ABA]
           
end_slow_loop = time.perf_counter()
print("first hydro loop took:", end_slow_loop-start_slow_loop)

start_slow_loop = time.perf_counter()

## hydro renewal and greenfield development necessary inputs
if hydro_development:
    if storage_continous and not just_small_hydro:
        hydro_new = pd.read_csv (r'hydro_new.csv',header=0)
    elif not just_small_hydro:
        hydro_new = pd.read_csv (r'hydro_new.csv',header=0)
    elif just_small_hydro:
        hydro_new = pd.read_csv (r'hydro_new.csv',header=0)
       
             
   
    hydro_renewal=list(hydro_new.iloc[:]['Short Name'])
    cost_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Annualized Capital Cost ($M/year)']))) ##For some reason this was taken out of hydro_new.csv -JGM 24 MAY 2023
    capacity_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Additional Capacity (MW)'])))
    devperiod_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Development Time (years)'])))
    location_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Balancing Area'])))
    distance_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Distance to Grid (km)'])))
    type_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Generation Type - COPPER'])))
    fixed_o_m_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Fixed O&M ($/MW-year)'])))
    variable_o_m_renewal=dict(zip(hydro_renewal,list(hydro_new.iloc[:]['Variable O&M ($/MWh)'])))
   
    hr_ror=list()
    cost_ror_renewal=dict()
    capacity_ror_renewal=dict()
    hr_ror_location=dict()
   
    hr_day=list()
    cost_day_renewal=dict()
    capacity_day_renewal=dict()
    hr_day_location=dict()
   
    hr_mo=list()
    cost_month_renewal=dict()
    capacity_month_renewal=dict()
    hr_month_location=dict()
   
    hr_pump=list()
    cost_pump_renewal=dict()
    capacity_pump_renewal=dict()
    hr_pump_location=dict()
    for k in hydro_renewal:
        if foryear-2020>=devperiod_renewal[k]:
            if type_renewal[k]=='hydro_run':
                hr_ror.append(k)
                cost_ror_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_ror_renewal[k]=capacity_renewal[k]
                hr_ror_location[k]=location_renewal[k]
   
            if type_renewal[k]=='hydro_daily':
                hr_day.append(k)
                cost_day_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_day_renewal[k]=capacity_renewal[k]
                hr_day_location[k]=location_renewal[k]
   
            if type_renewal[k]=='hydro_monthly':
                hr_mo.append(k)
                cost_month_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_month_renewal[k]=capacity_renewal[k]
                hr_month_location[k]=location_renewal[k]
            if type_renewal[k]=='storage_PH':
                hr_pump.append(k)
                cost_pump_renewal[k]=cost_renewal[k]*1000000+distance_renewal[k]*intra_ba_transcost*capacity_renewal[k]
                capacity_pump_renewal[k]=capacity_renewal[k]
                hr_pump_location[k]=location_renewal[k]
   
   
                 
               
    ror_renewalout=dict()
    for HR_ROR in hr_ror:
        for H in h:
            province_loc=hr_ror_location[HR_ROR].replace('.a','')
            province_loc=province_loc.replace('.b','')
            ror_renewalout[str(H)+'.'+HR_ROR]=capacity_ror_renewal[HR_ROR]*hydro_cf[province_loc+'.'+str(H)]
   
    day_renewal_historic=dict()
    day_renewalout=dict()
    for HR_DAY in hr_day:
        for D in d:
            day_renewal_historic[str(D)+'.'+HR_DAY]=0
        for H in h:
            province_loc=hr_day_location[HR_DAY].replace('.a','')
            province_loc=province_loc.replace('.b','')
            day_renewalout[str(H)+'.'+HR_DAY]=capacity_day_renewal[HR_DAY]*hydro_cf[province_loc+'.'+str(H)]
            day_renewal_historic[str(map_hd[H])+'.'+HR_DAY]=day_renewal_historic[str(map_hd[H])+'.'+HR_DAY]+day_renewalout[str(H)+'.'+HR_DAY]
   
    month_renewal_historic=dict()
    month_renewalout=dict()
    for HR_MO in hr_mo:
        for M in m:
            month_renewal_historic[str(M)+'.'+HR_MO]=0
        for H in h:
            province_loc=hr_month_location[HR_MO].replace('.a','')
            province_loc=province_loc.replace('.b','')
            month_renewalout[str(H)+'.'+HR_MO]=capacity_month_renewal[HR_MO]*hydro_cf[province_loc+'.'+str(H)]
            month_renewal_historic[str(map_hm[H])+'.'+HR_MO]=month_renewal_historic[str(map_hm[H])+'.'+HR_MO]+month_renewalout[str(H)+'.'+HR_MO]
   

end_slow_loop = time.perf_counter()
print("second hydro loop:", end_slow_loop-start_slow_loop)

#####recontract input data ###########33
windonscost_recon=dict()
windofscost_recon=dict()
solarcost_recon=dict()

for PD in pds:
    windonscost_recon[PD]= config_file['Economics']['windcost_recon_ons']*technology_evolution['wind_ons.'+PD]
    windofscost_recon[PD]= config_file['Economics']['windcost_recon_ofs']*technology_evolution['wind_ofs.'+PD]
    solarcost_recon[PD]= config_file['Economics']['solarcost_recon']*technology_evolution['solar.'+PD]

windsolarrecon = pd.read_csv(r'wind_solar_location_recon.csv',header=0)

wind_ons_recon_capacity=dict()
wind_ofs_recon_capacity=dict()
solar_recon_capacity=dict()
for PD in pds:
    for GL in gl:
        wind_ons_recon_capacity[PD+'.'+GL]=0
        wind_ofs_recon_capacity[PD+'.'+GL]=0
        solar_recon_capacity[PD+'.'+GL]=0
       
        if str(GL)+'.'+'wind_ons' in list(windsolarrecon[:]['location']):
            wind_ons_recon_capacity[PD+'.'+GL]=windsolarrecon[PD][list(windsolarrecon[:]['location']).index(str(GL)+'.'+'wind_ons')]
        if str(GL)+'.'+'wind_ofs' in list(windsolarrecon[:]['location']):
            wind_ofs_recon_capacity[PD+'.'+GL]=windsolarrecon[PD][list(windsolarrecon[:]['location']).index(str(GL)+'.'+'wind_ofs')]
        if str(GL)+'.'+'solar' in list(windsolarrecon[:]['location']):
            solar_recon_capacity[PD+'.'+GL]=windsolarrecon[PD][list(windsolarrecon[:]['location']).index(str(GL)+'.'+'solar')]
       

cleared_data=gc.collect()
######### Duplicating some sets ###############
ttplants=tplants
ggl=gl
hh=h
app=ap
abba=aba

end = time.time()
print(f'\n==================================================\n\
Initializing input data time (Sec): {round((end-start)/60)} Min and {round((end-start)%60)} Sec \
\n==================================================')
start=time.time()

model = ConcreteModel()
second_dispatch_flag = False
#def recreate_model():
########Creating the Optimization Model######################

#### Defining variables####

model.capacity_therm=Var( pds, aba,tplants, within=NonNegativeReals,initialize=1) #new thermal plant capacity in MW
#At definition of capacity therm
print(model.capacity_therm)
#At definition of capacity therm
print(tplants)
model.retire_therm=Var( pds, aba,tplants, within=NonNegativeReals,initialize=0)  #retire extant thermal capacity in MW
model.capacity_wind_ons=Var( pds, gl, within=NonNegativeReals,initialize=0)  #onshore wind plant capacity in MW
model.capacity_wind_ofs=Var( pds, gl, within=NonNegativeReals,initialize=0)  #offshore wind plant capacity in MW
model.capacity_solar=Var( pds, gl, within=NonNegativeReals,initialize=0)  #solar plant capacity in MW
if storage_continous:
    model.capacity_storage=Var(pds, st, aba, within=NonNegativeReals,initialize=0)  #storage plant capacity in MW

model.supply=Var(pds, h,aba,tplants, within=NonNegativeReals,initialize=0)  #fossil fuel supply in MW
model.windonsout=Var(pds, h,aba, within=NonNegativeReals,initialize=0)  #onshore wind hourly power output
model.windofsout=Var(pds, h,aba, within=NonNegativeReals,initialize=0)  #offshore wind hourly power output
model.solarout=Var(pds, h,aba, within=NonNegativeReals,initialize=0)  #solar hourly power output
model.load_shedding = Var(pds, h, aba, within=NonNegativeReals, initialize=0) #Hourly Load Shedding in MW
model.storageout=Var(pds,st,h,aba, within=NonNegativeReals,initialize=0)  #pumped hydro hourly output
model.storagein=Var(pds,st, h,aba, within=NonNegativeReals,initialize=0)  #pumped hydro hourly input
model.storageenergy=Var(pds,st, h,aba, within=NonNegativeReals,initialize=0)  #total stored pump hydro energy in MWh
model.daystoragehydroout=Var(pds, h,aba, within=NonNegativeReals,initialize=0)  #day storage hydro output in MW
model.monthstoragehydroout=Var(pds, h,aba, within=NonNegativeReals,initialize=0)  #month storage hydro output in MW
model.transmission=Var(pds, h,aba,abba, within=NonNegativeReals,initialize=0)  #hourly transmission in MW from ap,ba to apa,abba
model.capacity_transmission=Var(pds, aba,abba, within=NonNegativeReals,initialize=0)  #transmission capacity in MW from ap,aba to app,abba

#model.carbon=Var(ap,aba, within=NonNegativeReals,initialize=0)  #carbon emissions annual in Mt
if hydro_development:
    model.ror_renewal_binary=Var(pds, hr_ror, within=Binary,initialize=1)
    model.day_renewal_binary=Var(pds, hr_day, within=Binary,initialize=1)
    model.month_renewal_binary=Var(pds, hr_mo, within=Binary,initialize=1)
    model.dayrenewalout=Var(pds, h,hr_day, within=NonNegativeReals,initialize=0)
    model.monthrenewalout=Var(pds, h,hr_mo, within=NonNegativeReals,initialize=0)

    if not storage_continous:
        model.pumphydro=Var(pds, hr_pump, within=Binary,initialize=0)
model.capacity_wind_ons_recon=Var( pds, gl, within=NonNegativeReals,initialize=0)  #onshore wind recontract capacity in MW
model.capacity_wind_ofs_recon=Var( pds, gl, within=NonNegativeReals,initialize=0)  #offshore wind recontract capacity in MW
model.capacity_solar_recon=Var( pds, gl, within=NonNegativeReals,initialize=0)  #solar recontract capacity in MW

## Intializing storages' energy
for ABA in aba:
    for PD in pds:
        for ST in st:
            model.storageenergy[PD,ST,h[0],ABA].fix(0)

m_counter=2
for H in h:
    if map_hm[H]==m_counter:
        for ABA in aba:
            for PD in pds:
                for ST in st:
                    model.storageenergy[PD,ST,H,ABA].fix(0)
        m_counter+=1
        

###Objective function total cost minimization###
def obj_rule(model):
    capcost=dict()
    fcost=dict()
    fixedOM=dict()
    variableOM=dict()
    hydrorenewalccost=dict()
    hydrorenewalomcost=dict()
    newstorage_ccost=dict()
    newstorage_omcost=dict()
    tcapcost=0
    tfcost=0
    tfixedOM=0
    tvariableOM=0
    thydrorenewalccost=0
    thydrorenewalomcost=0
    tnewstorage_ccost=0
    tnewstorage_omcost=0
    for PD in pds:
        ind=pds.index(PD)
        if flag_itc:
            capcost[PD]=sum(model.capacity_therm[PDD,ABA,ITC]*capitalcost[ABA+'.'+ITC]* factor_itc *((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for ITC in ITCplants for ABA in aba if ITC in tplants and PDD in ['2025', '2030'])\
                +sum(model.capacity_therm[PDD,ABA,ITC]*capitalcost[ABA+'.'+ITC]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for ITC in ITCplants for ABA in aba if ITC in tplants and PDD in ['2035', '2040','2045', '2050'])\
                +sum(model.capacity_therm[PDD,ABA,TP]*capitalcost[ABA+'.'+TP]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for TP in tplants for ABA in aba if TP not in ITCplants)\
                +sum(model.capacity_wind_ons[PDD,GL] * factor_itc * windonscost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ofs[PDD,GL] * factor_itc * windofscost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_solar[PDD,GL] * factor_itc * solarcost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ons_recon[PDD,GL] * factor_itc * windonscost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ofs_recon[PDD,GL] * factor_itc * windofscost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_solar_recon[PDD,GL] * factor_itc * solarcost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_transmission[PDD,ABA,ABBA]* factor_itc *transcost*distance[ABA+'.'+ABBA]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for ABA in aba for ABBA in aba if ABA+'.'+ABBA in transmap)
        else:
            capcost[PD]=sum(model.capacity_therm[PDD,ABA,TP]*capitalcost[ABA+'.'+TP]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for TP in tplants for ABA in aba)\
                +sum(model.capacity_wind_ons[PDD,GL] * windonscost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ofs[PDD,GL] * windofscost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_solar[PDD,GL] * solarcost[PDD+'.'+GL]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ons_recon[PDD,GL] * windonscost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_wind_ofs_recon[PDD,GL] * windofscost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_solar_recon[PDD,GL] * solarcost_recon[PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for GL in gl)\
                +sum(model.capacity_transmission[PDD,ABA,ABBA]*transcost*distance[ABA+'.'+ABBA]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1]  for ABA in aba for ABBA in aba if ABA+'.'+ABBA in transmap)

        ##Fuel cost for thermal generators for first economic dispatch (supply) -Reza's original implementation
        fcost[PD]=sum(model.supply[PD,H,ABA,TP] * fuelcost[PD+'.'+TP+'.'+ABA] for H in h for ABA in aba for TP in tplants)*365/len(run_days)

        ##Fixed operation and maintenance costs for thermals, wind, solar, transmission, and hydro (no storage terms here)
        fixedOM[PD]=sum((extant_thermal[pds[0]+'.'+ABA+'.'+TP]+sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1]))* fixed_o_m[TP]  for ABA in aba for TP in tplants)\
            +sum((model.capacity_wind_ons[PDD,GL]+model.capacity_wind_ons_recon[PDD,GL])* fixed_o_m['wind_ons']  for PDD in pds[:ind+1] for GL in gl)\
            +sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ons'] * fixed_o_m['wind_ons']  for GL in gl if str(GL)+'.'+'wind_ons' in extant_wind_solar[0])\
            +sum((model.capacity_wind_ofs[PDD,GL]+model.capacity_wind_ofs_recon[PDD,GL])* fixed_o_m['wind_ofs']  for PDD in pds[:ind+1] for GL in gl)\
            +sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ofs'] * fixed_o_m['wind_ofs']  for GL in gl if str(GL)+'.'+'wind_ofs' in extant_wind_solar[0])\
            +sum((model.capacity_solar[PDD,GL]+model.capacity_solar_recon[PDD,GL])* fixed_o_m['solar']  for PDD in pds[:ind+1] for GL in gl)\
            +sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'solar'] * fixed_o_m['solar']  for GL in gl if str(GL)+'.'+'solar' in extant_wind_solar[0])\
            +sum(model.capacity_transmission[PDD,ABA,ABBA]*trans_o_m  for PDD in pds[:ind+1] for ABA in aba for ABBA in aba if ABA+'.'+ABBA in transmap)\
            +sum(extant_transmission[pds.index(PD)][ABA+'.'+ABBA]*trans_o_m  for ABA in aba for ABBA in aba if ABA+'.'+ABBA in extant_transmission[pds.index(PD)])\
            +sum(ror_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_run']  for ABA in aba)\
            +sum(day_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_daily']  for ABA in aba)\
            +sum(month_hydro_capacity[PD+'.'+ABA] * fixed_o_m['hydro_monthly']  for ABA in aba)
    
        ##Fixed operation and maintenance costs for thermals, wind, solar, transmission, and hydro for second economic dispatch (no storage terms here) -JGM
        ##This term is exactly equal to the fixed O&M from the first economic dispatch, although it happens a year later which is handled below

        ##Variable operation and maintenance cost for thermals, wind, solar, and hydro for first economic dispatch
        variableOM[PD]=sum(model.supply[PD,H,ABA,TP] * variable_o_m[TP]  for H in h for ABA in aba for TP in tplants)*365/len(run_days)\
            +sum(model.windonsout[PD,H,ABA]*variable_o_m['wind_ons']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(model.windofsout[PD,H,ABA]*variable_o_m['wind_ofs']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(model.solarout[PD,H,ABA]*variable_o_m['solar']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(ror_hydroout[PD+'.'+str(H)+'.'+ABA]*variable_o_m['hydro_run']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(model.daystoragehydroout[PD,H,ABA]*variable_o_m['hydro_daily']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(model.monthstoragehydroout[PD,H,ABA]*variable_o_m['hydro_monthly']  for H in h for ABA in aba)*365/len(run_days)\
            +sum(model.load_shedding[PD,H,ABA]*load_shedding_cost for ABA in aba for H in h )*365/len(run_days)

                #Need to add terms here for additional years of operation

        #Capital cost for hydro renewal  
        hydrorenewalccost[PD]=0
        #Operation & maintenance cost for hydro renewal for first economic dispatch
        hydrorenewalomcost[PD]=0
    
        if hydro_development:
            ##I suspect the functions below double/triple count capital cost, for PDD in pds[:ind+1] - JGM
            if flag_itc:
                hydrorenewalccost[PD]=sum(cost_ror_renewal[HR_ROR]* factor_itc *model.ror_renewal_binary[PDD,HR_ROR]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_ROR in hr_ror if PDD in ['2025', '2030'])\
                    +sum(cost_ror_renewal[HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_ROR in hr_ror if PDD in ['2035', '2040','2045', '2050'])\
                    +sum(cost_day_renewal[HR_DAY]* factor_itc *model.day_renewal_binary[PDD,HR_DAY]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_DAY in hr_day if PDD in ['2025', '2030'])\
                    +sum(cost_day_renewal[HR_DAY]*model.day_renewal_binary[PDD,HR_DAY]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_DAY in hr_day if PDD in ['2035', '2040','2045', '2050'])\
                    +sum(cost_month_renewal[HR_MO]* factor_itc *model.month_renewal_binary[PDD,HR_MO]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_MO in hr_mo if PDD in ['2025', '2030'])\
                    +sum(cost_month_renewal[HR_MO]*model.month_renewal_binary[PDD,HR_MO]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_MO in hr_mo if PDD in ['2035', '2040','2045', '2050'])
            else:
                hydrorenewalccost[PD]=sum(cost_ror_renewal[HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_ROR in hr_ror)\
                    +sum(cost_day_renewal[HR_DAY]*model.day_renewal_binary[PDD,HR_DAY]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_DAY in hr_day)\
                    +sum(cost_month_renewal[HR_MO]*model.month_renewal_binary[PDD,HR_MO]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for HR_MO in hr_mo)
            hydrorenewalomcost[PD]=sum(capacity_ror_renewal[HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR]*fixed_o_m_renewal[HR_ROR] for PDD in pds[:ind+1] for HR_ROR in hr_ror)\
                +sum(capacity_day_renewal[HR_DAY]*model.day_renewal_binary[PDD,HR_DAY]*fixed_o_m_renewal[HR_DAY] for PDD in pds[:ind+1] for HR_DAY in hr_day)\
                +sum(capacity_month_renewal[HR_MO]*model.month_renewal_binary[PDD,HR_MO]*fixed_o_m_renewal[HR_MO] for PDD in pds[:ind+1] for HR_MO in hr_mo)\
                +sum(ror_renewalout[str(H)+'.'+HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR]*variable_o_m_renewal[HR_ROR] for PDD in pds[:ind+1] for H in h for HR_ROR in hr_ror)*365/len(run_days)\
                +sum(model.dayrenewalout[PD,H,HR_DAY]*variable_o_m_renewal[HR_DAY]  for H in h for HR_DAY in hr_day)*365/len(run_days)\
                +sum(model.monthrenewalout[PD,H,HR_MO]*variable_o_m_renewal[HR_MO]  for H in h for HR_MO in hr_mo)*365/len(run_days)

            if not storage_continous:
                if flag_itc:
                    hydrorenewalccost[PD]+=sum(cost_pump_renewal[HR_PUMP]* factor_itc *model.pumphydro[PDD,HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump if PDD in ['2025', '2030'])
                    hydrorenewalccost[PD]+=sum(cost_pump_renewal[HR_PUMP]*model.pumphydro[PDD,HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump if PDD in ['2035', '2040','2045', '2050'])
                else:
                    hydrorenewalccost[PD]+=sum(cost_pump_renewal[HR_PUMP]*model.pumphydro[PDD,HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump)
                hydrorenewalomcost[PD]+=sum(capacity_pump_renewal[HR_PUMP]*model.pumphydro[PDD,HR_PUMP]*fixed_o_m_renewal[HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump)
                ##O&M cost for hydro renewal for second economic dispatch, should be equal to first economic dispatch -JGM

        newstorage_ccost[PD]=0
        newstorage_omcost[PD]=0

        if storage_continous:
            if flag_itc:
                newstorage_ccost[PD]=sum(model.capacity_storage[PDD,ST,ABA] * factor_itc * storage_cost[ST+'.'+PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for ST in st for ABA in aba if PDD in ['2025', '2030'])
                newstorage_ccost[PD]=sum(model.capacity_storage[PDD,ST,ABA] * storage_cost[ST+'.'+PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for ST in st for ABA in aba if PDD in ['2035', '2040','2045', '2050'])
            else:
                newstorage_ccost[PD]=sum(model.capacity_storage[PDD,ST,ABA] * storage_cost[ST+'.'+PDD]*((1+inflation)**(int(PDD)-2021)) for PDD in pds[:ind+1] for ST in st for ABA in aba)
            newstorage_omcost[PD]=sum(store_fix_o_m[ST+'.'+PDD] * model.capacity_storage[PDD,ST,ABA] for PDD in pds[:ind+1] for ST in st for ABA in aba)

        disc_coef=(1/(1+discount)**((int(PD)-1)-2021)) #New, we are using annuity series combined with future value to present value formula
        if ind != (len(pds)-1):
            #We use disc_ann_coef as the coeffecient to bring an n-year series Annuity to a net present value
            first_term = (1/(1+discount)**(int(pds[ind+1])-int(PD)))
            second_term = 1
            third_term = discount
            disc_ann_coef = ((second_term - first_term) / third_term)
            if (disc_ann_coef < 0):
                print(disc_ann_coef)
                sys.exit()
        disc_coef2=(1/(1+discount_2050)**(int(PD)-2021)) ##Use this for the final subperiod since we are not annualizing it -- treat it as a future to present value
        inf_coef=((1+inflation)**(int(PD)-2021))
        if ind == (len(pds)-1):
            tcapcost+=capcost[PD]*disc_coef2
            tfcost+=fcost[PD]*disc_coef2*inf_coef ##This is treating fuel cost like a single future value
            tvariableOM+=variableOM[PD]*disc_coef2*inf_coef
            tfixedOM+=fixedOM[PD]*disc_coef2*inf_coef
            thydrorenewalccost+=hydrorenewalccost[PD]*disc_coef2
            thydrorenewalomcost+=hydrorenewalomcost[PD]*disc_coef2*inf_coef
            tnewstorage_ccost+=newstorage_ccost[PD]*disc_coef2
            tnewstorage_omcost+=newstorage_omcost[PD]*disc_coef2*inf_coef
        else:
            tcapcost+=capcost[PD]*disc_ann_coef*disc_coef
            tfcost+=fcost[PD]*disc_ann_coef*disc_coef*inf_coef ##This is treating fuel cost like a single future value
            tvariableOM+=variableOM[PD]*disc_ann_coef*disc_coef*inf_coef
            tfixedOM+=fixedOM[PD]*disc_ann_coef*disc_coef*inf_coef
            thydrorenewalccost+=hydrorenewalccost[PD]*disc_ann_coef*disc_coef
            thydrorenewalomcost+=hydrorenewalomcost[PD]*disc_ann_coef*disc_coef*inf_coef
            tnewstorage_ccost+=newstorage_ccost[PD]*disc_ann_coef*disc_coef
            tnewstorage_omcost+=newstorage_omcost[PD]*disc_ann_coef*disc_coef*inf_coef
    return (tcapcost+tfcost+tvariableOM+thydrorenewalccost+thydrorenewalomcost+tnewstorage_ccost+tnewstorage_omcost+tfixedOM)
    
model.obj = Objective(rule=obj_rule,sense=minimize)

######Planning reserve requirement#####

def planning_reserve(model,PD,SEAS,AP):
    ind=pds.index(PD)
    cap_val=sum((extant_thermal[pds[0]+'.'+ABA+'.'+TP]+sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1] )) for ABA in  regions_per_province[AP] for TP in tplants)\
            +sum((model.capacity_wind_ons[PDD,GL]+model.capacity_wind_ons_recon[PDD,GL])*float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)])+sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind'] * float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]])  for GL in gl if str(GL)+'.'+'wind' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)] )\
            +sum((model.capacity_wind_ofs[PDD,GL]+model.capacity_wind_ofs_recon[PDD,GL])*float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) \
            +sum((model.capacity_solar[PDD,GL]+model.capacity_solar_recon[PDD,GL])*float(capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)])+sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'solar'] * float(capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for GL in gl if str(GL)+'.'+'solar' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)] )\
            +sum(ror_hydro_capacity[PD+'.'+ABA] for ABA in regions_per_province[AP])\
            +sum(day_hydro_capacity[PD+'.'+ABA] for ABA in  regions_per_province[AP])\
            +sum(month_hydro_capacity[PD+'.'+ABA] for ABA in  regions_per_province[AP])\

    if hydro_development:
        cap_val+=sum(capacity_ror_renewal[HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR] for PDD in pds[:ind+1] for HR_ROR in hr_ror)\
                +sum(capacity_day_renewal[HR_DAY]*model.day_renewal_binary[PDD,HR_DAY] for PDD in pds[:ind+1] for HR_DAY in hr_day)\
                +sum(capacity_month_renewal[HR_MO]*model.month_renewal_binary[PDD,HR_MO] for PDD in pds[:ind+1] for HR_MO in hr_mo)
    if storage_continous:
        cap_val+=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[:ind+1] for ST in st for ABA in  regions_per_province[AP])

    #tokens = ABA.split(".", 1)    
    return cap_val >= peak_demand[PD+'.'+SEAS+'.'+AP]*(float(reserve_margin_dict[AP])+1)
model.planning_reserve=Constraint(pds,season,ap, rule=planning_reserve)

###constrain retirements to extant (or existing) plants
def retire(model,PD,ABA,TP):
    ind=pds.index(PD)
    return model.retire_therm[PD,ABA,TP] <= extant_thermal[pds[0]+'.'+ABA+'.'+TP]+sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind])
model.retire=Constraint(pds,aba,tplants, rule=retire)

#### forces the model to retire the plants that are at end of life
def lifetime(model,PD,ABA,TP):
    ind=pds.index(PD)
    ex_thermal=extant_thermal[PD+'.'+ABA+'.'+TP]

    #######for under construction units, this will prevent infeasibility
    if ind>=1:
        if extant_thermal[PD+'.'+ABA+'.'+TP]-extant_thermal[pds[ind-1]+'.'+ABA+'.'+TP]>0:
            ex_thermal=100000

    return extant_thermal[pds[0]+'.'+ABA+'.'+TP]-sum(model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1]) <= ex_thermal
model.lifetime=Constraint(pds,aba,tplants, rule=lifetime)

#OA
###wind generation limit
def windonsg(model,PD,H,ABA):
    ind=pds.index(PD)
    return model.windonsout[PD,H,ABA]<=sum((model.capacity_wind_ons[PDD,GL]+model.capacity_wind_ons_recon[PDD,GL])*windcf_df.loc[H,int(GL)] for PDD in pds[:ind+1] for GL in gl if ABA==map_gl_to_ba[int(GL)])+ extant_wind_ons_gen.loc[(PD,H),ABA]
#OA
def windofsg(model,PD,H,ABA):
    ind=pds.index(PD)
    return model.windofsout[PD,H,ABA]<=sum((model.capacity_wind_ofs[PDD,GL]+model.capacity_wind_ofs_recon[PDD,GL])*windcf_df.loc[H,int(GL)] for PDD in pds[:ind+1] for GL in gl if ABA==map_gl_to_ba[int(GL)])+ extant_wind_ofs_gen.loc[(PD,H),ABA]

model.windonsg=Constraint(pds,h,aba, rule=windonsg)
model.windofsg=Constraint(pds,h,aba, rule=windofsg)

###wind generation limit 2 -JGM
def windonsg2(model,PD,H,ABA):
    ind=pds.index(PD)
    return model.windonsout2[PD,H,ABA]<=sum((model.capacity_wind_ons[PDD,GL]+model.capacity_wind_ons_recon[PDD,GL])*windcf_df.loc[H,int(GL)] for PDD in pds[:ind+1] for GL in gl if ABA==map_gl_to_ba[int(GL)])+ extant_wind_ons_gen.loc[(PD,H),ABA]
#OA
def windofsg2(model,PD,H,ABA):
    ind=pds.index(PD)
    return model.windofsout2[PD,H,ABA]<=sum((model.capacity_wind_ofs[PDD,GL]+model.capacity_wind_ofs_recon[PDD,GL])*windcf_df.loc[H,int(GL)] for PDD in pds[:ind+1] for GL in gl if ABA==map_gl_to_ba[int(GL)])+ extant_wind_ofs_gen.loc[(PD,H),ABA]

if second_dispatch_flag:
    model.windonsg2=Constraint(pds,h,aba, rule=windonsg2)
    model.windofsg2=Constraint(pds,h,aba, rule=windofsg2)
####solar generation limit
def solarg(model,PD,H,ABA):
    ind=pds.index(PD)
    return model.solarout[PD,H,ABA]<=sum(
        (
            model.capacity_solar[PDD,GL] 
            + model.capacity_solar_recon[PDD,GL])*solarcf_df.loc[H,int(GL)] for PDD in pds[:ind+1] \
                for GL in gl if ABA==map_gl_to_ba[int(GL)])\
            + extant_solar_gen.loc[(PD,H),ABA]
model.solarg=Constraint(pds,h,aba, rule=solarg)


#Deliting windcf and solarcf to free some memory
del windcf_df
del solarcf_df

#Limiting offshore wind reconstruction capacity to existing end of life offshore wind capacity
def wind_ofs_recon(model,PD,GL):
    ind=pds.index(PD)
    return model.capacity_wind_ofs_recon[PD,GL]<=wind_ofs_recon_capacity[PD+'.'+GL]+sum(wind_ofs_recon_capacity[PDD+'.'+GL]-model.capacity_wind_ofs_recon[PDD,GL] for PDD in pds[:ind])
model.wind_ofs_recon=Constraint(pds,gl, rule=wind_ofs_recon)

#Limiting onshore wind reconstruction capacity to existing end of life onshore wind capacity
def wind_ons_recon(model,PD,GL):
    ind=pds.index(PD)
    return model.capacity_wind_ons_recon[PD,GL]<=wind_ons_recon_capacity[PD+'.'+GL]+sum(wind_ons_recon_capacity[PDD+'.'+GL]-model.capacity_wind_ons_recon[PDD,GL] for PDD in pds[:ind])
model.wind_ons_recon=Constraint(pds,gl, rule=wind_ons_recon)

#Limiting solar reconstruction capacity to existing end of life solar capacity
def solar_recon(model,PD,GL):
    ind=pds.index(PD)
    return model.capacity_solar_recon[PD,GL]<=solar_recon_capacity[PD+'.'+GL]+sum(solar_recon_capacity[PDD+'.'+GL]-model.capacity_solar_recon[PDD,GL] for PDD in pds[:ind])
model.solar_recon=Constraint(pds,gl, rule=solar_recon)

# Load shedding constraint for planning reserve (inclusion of us demand)
def loadShedding(model,PD,H,ABA):
    aux=[1]
    return model.load_shedding[PD,H, ABA]<=demand_df.loc[(PD,H),ABA]+sum(demand_us[ABA+'.'+str(H)] for i in aux if ABA+'.'+str(H) in demand_us)
model.loadShedding=Constraint(pds, h, aba, rule=loadShedding)



# DR - Autarky is to be implemented
##provincial supply and demand balance

'''
if autarky:
    def autarky(model,PD,AP):
    
        TP_supply=sum(model.supply[PD,H,ABA,TP] for H in h for ABA in aba for TP in tplants if AP in ABA)
        wind_solar_supply=sum(model.windofsout[PD,H,ABA] for H in h for ABA in aba if AP in ABA)\
            +sum(model.solarout[PD,H,ABA] for H in h for ABA in aba if AP in ABA)\
            +sum(model.windonsout[PD,H,ABA] for H in h for ABA in aba if AP in ABA)
        hydro_supply=sum(ror_hydroout[PD+'.'+str(H)+'.'+ABA] for H in h for ABA in aba if AP in ABA)\
            +sum(model.daystoragehydroout[PD,H,ABA] for H in h for ABA in aba if AP in ABA)\
            +sum(model.monthstoragehydroout[PD,H,ABA] for H in h for ABA in aba if AP in ABA)
        return TP_supply+wind_solar_supply+hydro_supply >=autonomy_pct[AP]*sum(demand[PD+'.'+ABA+'.'+str(H)] for ABA in aba for H in h if AP in ABA)
    model.autarky=Constraint(pds,ap, rule=autarky)
'''
#OA
##you will express supply and demand twice, once for the first year then again for the second year
###supply and demand balance
aux=[1]
def demsup(model,PD,H,ABA):
    TP_supply=sum(model.supply[PD,H,ABA,TP] for TP in tplants)
    wind_solar_supply=model.windonsout[PD,H,ABA]+model.windofsout[PD,H,ABA]+model.solarout[PD,H,ABA]
    hydro_supply=ror_hydroout[PD+'.'+str(H)+'.'+ABA]+model.daystoragehydroout[PD,H,ABA]+model.monthstoragehydroout[PD,H,ABA]
    storage_supply=sum(model.storageout[PD,ST,H,ABA]-model.storagein[PD,ST,H,ABA] for ST in st)
    renewal_supply=0
    if hydro_development:
        renewal_supply=sum(ror_renewalout[str(H)+'.'+HR_ROR]*model.ror_renewal_binary[PD,HR_ROR] for HR_ROR in hr_ror if ABA==hr_ror_location[HR_ROR])\
            +sum(model.dayrenewalout[PD,H,HR_DAY] for HR_DAY in hr_day if ABA==hr_day_location[HR_DAY])\
            +sum(model.monthrenewalout[PD,H,HR_MO] for HR_MO in hr_mo if ABA==hr_month_location[HR_MO])
    return TP_supply+wind_solar_supply+hydro_supply+storage_supply+renewal_supply>=demand_df.loc[(PD,H),ABA]+sum(demand_us[ABA+'.'+str(H)] for i in aux if ABA+'.'+str(H) in demand_us)-model.load_shedding[PD, H, ABA]\
                                                                        +sum(model.transmission[PD,H,ABA,ABBA]-(1-transloss[ABBA+'.'+ABA])*model.transmission[PD,H,ABBA,ABA] for ABBA in aba if ABA+'.'+ABBA in transmap)
model.demsup=Constraint(pds,h,aba, rule=demsup)

##Copying the set of hours so I can perform the calculation below
h_pen = h

##### Constraining fossil emitting thermal plants generation to zero if their emission intensities are over the limit - We are skipping regulation of CCS gas tech in 2035 - JGM
def below_threshold(model,PD,H,ABA,FTP):
    if (FTP == 'gasSC_ccs_pre2025' or FTP == 'gasCCS_post2025' or FTP == 'gasCC_ccs_pre2025' or FTP == 'gasCG_restricted_ccs_pre2025') and PD == '2035':
        return Constraint.Skip
    else:
        return model.supply[PD,H,ABA,FTP] <= 0
if flag_cer:
    model.below_threshold=Constraint(['2035', '2040', '2045', '2050'],h,aba,ftplants, rule=below_threshold)
### maximum annual emissions for backup thermal plants -JGM
def maxbackupemissions(model,PD,ABA,BG):
    ind=pds.index(PD)
    return sum(model.supply[PD,H,ABA,BG]*carbondioxide[BG] for H in h)*(365/len(rundays))<=(sum(model.capacity_therm[PDD,ABA,BG]-model.retire_therm[PDD,ABA,BG] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+BG])*max_em_backup
if flag_cer:
    model.maxbackupemissions=Constraint(['2035', '2040', '2045', '2050'],aba,backup_generators, rule=maxbackupemissions)
###maximum annual capacity factor for thermal plants
def maxcapfactor(model,PD,ABA,TP):
    ind=pds.index(PD)
    return sum(model.supply[PD,H,ABA,TP] for H in h)<=(sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+TP])*hours * max_cap_fact[TP]
model.maxcapfactor=Constraint(pds,aba,tplants, rule=maxcapfactor)

###minimum annual capacity factor for thermal plants  - Removed

'''def mincapfactor(model,PD,ABA,TP):
    ind=pds.index(PD)
    return sum(model.supply[PD,H,ABA,TP] for H in h)>=(sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+TP])*hours * min_cap_fact[TP]
model.mincapfactor=Constraint(pds,aba,tplants, rule=mincapfactor)'''

##transmission capacity constraint
def transcap(model,PD,H,ABA,ABBA):
    ind=pds.index(PD)
    return model.transmission[PD,H,ABA,ABBA]<=sum(model.capacity_transmission[PDD,ABA,ABBA] for PDD in pds[:ind+1] for i in aux if ABA+'.'+ABBA in transmap)+sum(extant_transmission[pds.index(PD)][ABA+'.'+ABBA] for i in aux if ABA+'.'+ABBA in extant_transmission[pds.index(PD)])
model.transcap=Constraint(pds,h,aba,abba, rule=transcap)

if CTE_extant:
    def TEcap(model,ABA,ABBA):
        return sum(model.capacity_transmission[PD,ABA,ABBA] for PD in pds)<= CTE_coef*sum(extant_transmission[0][ABA+'.'+ABBA] for i in aux if ABA+'.'+ABBA in extant_transmission[0])
    model.TEcap=Constraint(aba,abba, rule=TEcap)
elif CTE_custom:
    def TEcap(model,ABA,ABBA):
        print('inside the transmission cap constraint')
        print('***')
        print('***')
        print('***')
        if ABA+'.'+ABBA in trans_expansion_limits:
            TEL=trans_expansion_limits[ABA+'.'+ABBA]
        else:
            TEL=100000
            
        return sum(model.capacity_transmission[PD,ABA,ABBA] for PD in pds)<= TEL

    model.TEcap=Constraint(aba,abba, rule=TEcap)

#####capacity constraints for thermal plants
def cap(model,PD,H,ABA,TP):
    ind=pds.index(PD)
    return model.supply[PD,H,ABA,TP]<=sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+TP]
model.cap=Constraint(pds,h,aba,tplants, rule=cap)

### this constraint limits the PHS retrofit capacity to percentage of available hydro reservior facility in each BA
if storage_continous:
    def pumpretrofitlimit(model,PD,ABA):
        ind=pds.index(PD)
        return sum(model.capacity_storage[PDD,'storage_PH',ABA] for PDD in pds[:ind+1])<=(day_hydro_capacity[pds[0]+'.'+ABA]+month_hydro_capacity[pds[0]+'.'+ABA])*pump_ret_limit
    model.pumpretrofitlimit=Constraint(pds,aba, rule=pumpretrofitlimit)


#OA
#####pumped hydro energy storage - energy balance constraint
def pumpen(model,PD,ST,H,ABA):
    return model.storageenergy[PD,ST,H+time_diff[H],ABA]==model.storageenergy[PD,ST,H,ABA]-model.storageout[PD,ST,H,ABA]+model.storagein[PD,ST,H,ABA]*storage_efficiency[ST]
model.pumpen=Constraint(pds,st,h2,aba, rule=pumpen)

######pumped hydro energy storage
def pumcap(model,PD,ST,H,ABA):
    ind=pds.index(PD)
    pump_new_con=0
    pump_integer_cap=0

    if storage_continous:
        if ST=='storage_LI' and int(PD)>=2040:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[ind-2:ind+1])
        else:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[:ind+1])
    if hydro_development and not storage_continous:
        pump_integer_cap=sum(model.pumphydro[PDD,HR_PUMP]*capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump if hr_pump_location[HR_PUMP]==ABA)
    return model.storageenergy[PD,ST,H,ABA]<=(ba_storage_capacity[ABA+'.'+ST]+pump_integer_cap+pump_new_con)*storage_hours[ST]
model.pumcap=Constraint(pds,st,h,aba, rule=pumcap)

#OA
######pump hydro power capacity - maximum storage out
def storageoutmax(model,PD,ST,H,ABA):
    ind=pds.index(PD)
    pump_new_con=0
    pump_integer_cap=0

    if storage_continous:
        if ST=='storage_LI' and int(PD)>=2040:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[ind-2:ind+1])
        else:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[:ind+1])            
    if hydro_development and not storage_continous:
        pump_integer_cap=sum(model.pumphydro[PDD,HR_PUMP]*capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump if hr_pump_location[HR_PUMP]==ABA)
    return model.storageout[PD,ST,H,ABA]<=ba_storage_capacity[ABA+'.'+ST]+pump_integer_cap+pump_new_con
model.storageoutmax=Constraint(pds,st,h,aba, rule=storageoutmax)

#####pump hydro storage capacity - maximum storage in
def storageinmax(model,PD,ST,H,ABA):
    ind=pds.index(PD)

    pump_new_con=0
    pump_integer_cap=0

    if storage_continous:
        if ST=='storage_LI' and int(PD)>=2040:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[ind-2:ind+1])
        else:
            pump_new_con=sum(model.capacity_storage[PDD,ST,ABA] for PDD in pds[:ind+1])
    if hydro_development and not storage_continous:
        pump_integer_cap=sum(model.pumphydro[PDD,HR_PUMP]*capacity_pump_renewal[HR_PUMP] for PDD in pds[:ind+1] for HR_PUMP in hr_pump if hr_pump_location[HR_PUMP]==ABA)

    return model.storagein[PD,ST,H,ABA]<=ba_storage_capacity[ABA+'.'+ST]+pump_integer_cap+pump_new_con
model.storageinmax=Constraint(pds,st,h,aba, rule=storageinmax)

##### forces the first hour of months energy to zero
m_counter=2
fhom=list()
for H in h:
    if map_hm[H]==m_counter:
        fhom.append(H)
        m_counter+=1
    
def storage_rest_monthly(model,PD,ST,FHOM,ABA):
    return model.storageenergy[PD,ST,FHOM,ABA]==0
model.storage_rest_monthly=Constraint(pds,st,fhom,aba, rule=storage_rest_monthly)

#OA
#####hydro storage for systems with intra-day storage
def hydro_daystorage(model,PD,D,ABA):
    return sum(model.daystoragehydroout[PD,H,ABA] for H in h if map_hd[H]==D)<=day_hydro_historic[PD+'.'+str(D)+'.'+ABA]
model.hydro_daystorage=Constraint(pds,d,aba, rule=hydro_daystorage)

if hydro_development:
    #OA
    def hydro_dayrenewal(model,PD,D,HR_DAY):
        return sum(model.dayrenewalout[PD,H,HR_DAY] for H in h if map_hd[H]==D)<=day_renewal_historic[str(D)+'.'+HR_DAY]
    model.hydro_dayrenewal=Constraint(pds,d,hr_day, rule=hydro_dayrenewal)

#OA
###hydro storage for systems with intra-month storage
def hydro_monthstorage(model,PD,M,ABA):
    return sum(model.monthstoragehydroout[PD,H,ABA] for H in h if map_hm[H]==M)<=month_hydro_historic[PD+'.'+str(M)+'.'+ABA]
model.hydro_monthstorage=Constraint(pds,m,aba, rule=hydro_monthstorage)

if hydro_development:
    #OA
    def hydro_monthrenewal(model,PD,M,HR_MO):
        return sum(model.monthrenewalout[PD,H,HR_MO] for H in h if map_hm[H]==M)<=month_renewal_historic[str(M)+'.'+HR_MO]
    model.hydro_monthrenewal=Constraint(pds,m,hr_mo, rule=hydro_monthrenewal)


####hydro minimum flow constraints for systems with intra-day storage
def hydro_dayminflow(model,PD,H,ABA):
    return model.daystoragehydroout[PD,H,ABA]>=day_minflow[PD+'.'+ABA]
model.hydro_dayminflow=Constraint(pds,h,aba, rule=hydro_dayminflow)

if hydro_development:
    def renewal_dayminflow(model,PD,H,HR_DAY):
        return model.dayrenewalout[PD,H,HR_DAY]>=capacity_day_renewal[HR_DAY]*hydro_minflow*model.day_renewal_binary[PD,HR_DAY]
    model.renewal_dayminflow=Constraint(pds,h,hr_day, rule=renewal_dayminflow)

####hydro minimum flow constraints for systems with intra-month storage
def hydro_monthminflow(model,PD,H,ABA):
    return model.monthstoragehydroout[PD,H,ABA]>=month_minflow[PD+'.'+ABA]
model.hydro_monthminflow=Constraint(pds,h,aba, rule=hydro_monthminflow)

if hydro_development:
    def renewal_monthminflow(model,PD,H,HR_MO):
        return model.monthrenewalout[PD,H,HR_MO]>=capacity_month_renewal[HR_MO]*hydro_minflow*model.month_renewal_binary[PD,HR_MO]
    model.renewal_monthminflow=Constraint(pds,h,hr_mo, rule=renewal_monthminflow)

##hydro capacity constraints for systems with intra-day storage
def hydro_daycap(model,PD,H,ABA):
    return model.daystoragehydroout[PD,H,ABA]<=day_hydro_capacity[PD+'.'+ABA]
model.hydro_daycap=Constraint(pds,h,aba, rule=hydro_daycap)

if hydro_development:
    #OA
    def renewal_daycap(model,PD,H,HR_DAY):
        return model.dayrenewalout[PD,H,HR_DAY]<=capacity_day_renewal[HR_DAY]*model.day_renewal_binary[PD,HR_DAY]
    model.renewal_daycap=Constraint(pds,h,hr_day, rule=renewal_daycap)

#OA
#####hydro capacity constraints for systems with intra-month storage
def hydro_monthcap(model,PD,H,ABA):
    return model.monthstoragehydroout[PD,H,ABA]<=month_hydro_capacity[PD+'.'+ABA]
model.hydro_monthcap=Constraint(pds,h,aba, rule=hydro_monthcap)

if hydro_development:
    #OA
    def renewal_monthcap(model,PD,H,HR_MO):
        return model.monthrenewalout[PD,H,HR_MO]<=capacity_month_renewal[HR_MO]*model.month_renewal_binary[PD,HR_MO]
    model.renewal_monthcap=Constraint(pds,h,hr_mo, rule=renewal_monthcap)

###End of second economic dispatch constraints

##### The following constraints ensure that the model does not build a hydro renewal or greenfield project more than one time during all periods (pds)
if hydro_development:
    def ror_onetime(model, PD, HR_ROR):
        return (model.ror_renewal_binary[PD, HR_ROR]
                >= model.ror_renewal_binary[pds[pds.index(PD) - 1], HR_ROR])
    model.ror_onetime = Constraint(pds[1:], hr_ror, rule=ror_onetime)

    def day_onetime(model, PD, HR_DAY):
        return (model.day_renewal_binary[PD, HR_DAY]
                >= model.day_renewal_binary[pds[pds.index(PD) - 1], HR_DAY])
    model.day_onetime = Constraint(pds[1:], hr_day, rule=day_onetime)

    def month_onetime(model, PD, HR_MO):
        return (model.month_renewal_binary[PD, HR_MO]
                >= model.month_renewal_binary[pds[pds.index(PD) - 1], HR_MO])
    model.month_onetime = Constraint(pds[1:], hr_mo, rule=month_onetime)

#up ramp limit
def ramp_up (model,PD,H,ABA,TP):
    ind=pds.index(PD)
    return model.supply[PD,H+time_diff[H],ABA,TP]<=model.supply[PD,H,ABA,TP]+(sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+TP])*ramp_rate_percent[TP]*time_diff[H]
model.ramp_up=Constraint(pds,h2,aba,['nuclear'], rule=ramp_up)

#down ramp limit
def ramp_down (model,PD,H,ABA,TP):
    ind=pds.index(PD)
    return model.supply[PD,H+time_diff[H],ABA,TP]>=model.supply[PD,H,ABA,TP]-(sum(model.capacity_therm[PDD,ABA,TP]-model.retire_therm[PDD,ABA,TP] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+TP])*ramp_rate_percent[TP]*time_diff[H]
model.ramp_down=Constraint(pds,h2,aba,['nuclear'], rule=ramp_down)


#capacity limit for wind plants
def windofscaplimit (model,GL):
    return sum(model.capacity_wind_ofs[PD,GL]+model.capacity_wind_ofs_recon[PD,GL] for PD in pds) <= maxwindofs[GL]
model.windsofscaplimit=Constraint(gl, rule=windofscaplimit)

def windonscaplimit (model,GL):
    return sum(model.capacity_wind_ons[PD,GL]+model.capacity_wind_ons_recon[PD,GL] for PD in pds) <= maxwindons[GL]
model.windsonscaplimit=Constraint(gl, rule=windonscaplimit)
##Setting the pre-2025 thermal capacity balance - Transitioning capacity must pick ccs, fuel blending, backup, or retire.

def diesel_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"diesel_retire_pre2025"], model.capacity_therm[PD,ABA,"diesel_backup_pre2025"], model.capacity_therm[PD,ABA,"diesel_bio_pre2025"]]) <= model.retire_therm[PD,ABA,'diesel_pre2025']
model.diesel_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=diesel_pre2025_capacity_loss_over_period_less)
def diesel_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"diesel_retire_pre2025"], model.capacity_therm[PD,ABA,"diesel_backup_pre2025"], model.capacity_therm[PD,ABA,"diesel_bio_pre2025"]]) >= model.retire_therm[PD,ABA,'diesel_pre2025']
model.diesel_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=diesel_pre2025_capacity_loss_over_period_more)

def gas_cc_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCC_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasCC_rng_pre2025"], model.capacity_therm[PD,ABA,"gasCC_retire_pre2025"], model.capacity_therm[PD,ABA,"gasCC_backup_pre2025"]]) >= model.retire_therm[PD,ABA,'gasCC_pre2025']
model.gas_cc_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=gas_cc_pre2025_capacity_loss_over_period_more)
def gas_cc_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCC_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasCC_rng_pre2025"], model.capacity_therm[PD,ABA,"gasCC_retire_pre2025"], model.capacity_therm[PD,ABA,"gasCC_backup_pre2025"]]) <= model.retire_therm[PD,ABA,'gasCC_pre2025']
model.gas_cc_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=gas_cc_pre2025_capacity_loss_over_period_less)


def coal_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"coal_ccs_pre2025"], model.capacity_therm[PD,ABA,"coal_retire_pre2025"]]) >= model.retire_therm[PD,ABA,'coal_pre2025']
model.coal_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=coal_pre2025_capacity_loss_over_period_more)
def coal_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"coal_ccs_pre2025"], model.capacity_therm[PD,ABA,"coal_retire_pre2025"]]) <= model.retire_therm[PD,ABA,'coal_pre2025']
model.coal_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=coal_pre2025_capacity_loss_over_period_less)


def gas_sc_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasSC_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasSC_rng_pre2025"], model.capacity_therm[PD,ABA,"gasSC_retire_pre2025"], model.capacity_therm[PD,ABA,"gasSC_backup_pre2025"]]) >= model.retire_therm[PD,ABA,'gasSC_pre2025']
model.gas_sc_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=gas_sc_pre2025_capacity_loss_over_period_more)
def gas_sc_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasSC_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasSC_rng_pre2025"], model.capacity_therm[PD,ABA,"gasSC_retire_pre2025"], model.capacity_therm[PD,ABA,"gasSC_backup_pre2025"]]) <= model.retire_therm[PD,ABA,'gasSC_pre2025']
model.gas_sc_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=gas_sc_pre2025_capacity_loss_over_period_less)
def gas_cg_restricted_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCG_restricted_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_rng_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_retire_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_backup_pre2025"]]) >= model.retire_therm[PD,ABA,'gasCG_restricted_pre2025']
model.gas_cg_restricted_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=gas_cg_restricted_pre2025_capacity_loss_over_period_more)
def gas_cg_restricted_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCG_restricted_ccs_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_rng_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_retire_pre2025"], model.capacity_therm[PD,ABA,"gasCG_restricted_backup_pre2025"]]) <= model.retire_therm[PD,ABA,'gasCG_restricted_pre2025']
model.gas_cg_restricted_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=gas_cg_restricted_pre2025_capacity_loss_over_period_less)

def gas_cg_free_pre2025_capacity_loss_over_period_more(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCG_free_retire_pre2025"]]) >= model.retire_therm[PD,ABA,'gasCG_free_pre2025']
model.gas_cg_free_pre2025_capacity_loss_over_period_more=Constraint(pds, aba, rule=gas_cg_free_pre2025_capacity_loss_over_period_more)
def gas_cg_free_pre2025_capacity_loss_over_period_less(model, PD, ABA):
    return sum([model.capacity_therm[PD,ABA,"gasCG_free_retire_pre2025"]]) <= model.retire_therm[PD,ABA,'gasCG_free_pre2025']
model.gas_cg_free_pre2025_capacity_loss_over_period_less=Constraint(pds, aba, rule=gas_cg_free_pre2025_capacity_loss_over_period_less)


#one for =< and another for >= for each capacity equality

###Constrain generation of all cogeneration - JGM

def cg_limit_supply(model,PD,H,ABA,CG):
    ind=pds.index(PD)
    return model.supply[PD,H,ABA,CG]<=(sum(model.capacity_therm[PDD,ABA,CG]-model.retire_therm[PDD,ABA,CG] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+CG])*0.37
model.cg_limit_supply=Constraint(pds,h,aba,cg_tech, rule=cg_limit_supply)


##Constrain generation of retired generators to zero for first economic dispatch
def retire_no_gen(model,PD, H, ABA, RTP):
    return model.supply[PD,H,ABA,RTP] <= 0
model.retire_no_gen=Constraint(pds, h, aba, retired_generators, rule=retire_no_gen)

##Constraints to restrict development of pre2025 fossil generators - JGM

def diesel_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"diesel_pre2025"] for PD in pds) <= 0
model.diesel_capacity_no_more_pre_2025=Constraint(pds, aba, rule=diesel_capacity_no_more_pre_2025)

def coal_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"coal_pre2025"] for PD in pds) <= 0
model.coal_capacity_no_more_pre_2025=Constraint(pds, aba, rule=coal_capacity_no_more_pre_2025)

def gasSC_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"gasSC_pre2025"] for PD in pds) <= 0
model.gasSC_capacity_no_more_pre_2025=Constraint(pds, aba, rule=gasSC_capacity_no_more_pre_2025)

def gasCC_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"gasCC_pre2025"] for PD in pds) <= 0
model.gasCC_capacity_no_more_pre_2025=Constraint(pds, aba, rule=gasCC_capacity_no_more_pre_2025)

def gasCG_free_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"gasCG_free_pre2025"] for PD in pds) <= 0
model.gasCG_free_capacity_no_more_pre_2025=Constraint(pds, aba, rule=gasCG_free_capacity_no_more_pre_2025)

def gasCG_restricted_capacity_no_more_pre_2025(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"gasCG_restricted_pre2025"] for PD in pds) <= 0
model.gasCG_restricted_capacity_no_more_pre_2025=Constraint(pds, aba, rule=gasCG_restricted_capacity_no_more_pre_2025)

##The constraint below is intended to eliminate backup generation capacity, for the business as usual case
def no_backup_generators(model, PD, ABA, BG):
    return sum(model.capacity_therm[PD,ABA,BG] for PD in pds) <= 0
if flag_cer == False:
    model.no_backup_generators=Constraint(pds, aba, backup_generators, rule=no_backup_generators)

def gasCC_free_capacity_no_post_2025_CER(model, PD, ABA):
    return sum(model.capacity_therm[PD,ABA,"gasCC_free_post2025"] for PD in pds) <= 0
if flag_cer:
    model.gasCC_free_capacity_no_post_2025_CER=Constraint(pds, aba, rule=gasCC_free_capacity_no_post_2025_CER)

#capacity limit for solar plants
def solarcaplimit (model,GL):
    return sum(model.capacity_solar[PD,GL]+model.capacity_solar_recon[PD,GL] for PD in pds) <= maxsolar[GL]
model.solarcaplimit=Constraint(gl, rule=solarcaplimit)

###################carbon limit constraint can be on or off##################
if provincial_emission_limit:
    def provincialcarbonlimit(model,AP):
        return sum(model.supply[pds[-1],H,ABA,TP]*carbondioxide[TP]/1000000 for H in h for TP in tplants for ABA in aba if AP in ABA)<=carbon_limit[AP]/(365/len(rundays))
    model.provincialcarbonlimit=Constraint(ap, rule=provincialcarbonlimit)

if national_emission_limit:
    def nationalcarbonlimit(model,PD):
        return sum(model.supply[PD,H,ABA,TP]*carbondioxide[TP]/1000000 for H in h for TP in tplants for ABA in aba)<=nat_em_limit[PD]/(365/len(rundays))
    model.nationalcarbonlimit=Constraint(pds, rule=nationalcarbonlimit)

if min_installed_LB_PHP:
    def installedgasPHP(model,ABA):
        return sum(model.capacity_storage[PD,ST,ABA] for PD in pds for ST in st)+ba_storage_capacity[ABA+'.storage_PH']>=LB_PHP_installed_limit[ABA]
    model.installedgasPHP=Constraint(aba, rule=installedgasPHP)

if new_thermal_limit:
    def newthermallimit(model,PD,LTP):
        return sum(model.capacity_therm[PD,ABA,LTP] for ABA in aba)<=0
    model.newthermallimit=Constraint(pds,limited_tplants, rule=newthermallimit)

if thermal_phase_out:
    #OA
    def phaseout(model,PHOT,ABA):
        ind=pds.index(phase_out_type_year[PHOT])
        return extant_thermal[pds[0]+'.'+ABA+'.'+PHOT]+sum(model.capacity_therm[PDD,ABA,PHOT]-model.retire_therm[PDD,ABA,PHOT] for PDD in pds[:ind+1]) <= 0
    model.phaseout=Constraint(ph_out_t,aba, rule=phaseout)    

###Maximum biomass capacity (By Balancing Area)

if max_bio_aba:

    def maxbioaba(model, PD, ABA):
        ind=pds.index(PD)
        return extant_thermal[pds[0]+'.'+ABA+'.'+'biomass']+sum(model.capacity_therm[PDD,ABA,'biomass']-model.retire_therm[PDD,ABA,'biomass'] for PDD in pds[:ind+1]) <= max_bio[ABA]
    model.maxbioaba=Constraint(pds, aba, rule=maxbioaba)

###Maximum nuclear capacity (By Balancing Area)

if max_nuclear_aba:

    def maxnuclearaba(model, PD, ABA):
        ind=pds.index(PD)
        return extant_thermal[pds[0]+'.'+ABA+'.'+'nuclear']+sum(model.capacity_therm[PDD,ABA,'nuclear']-model.retire_therm[PDD,ABA,'nuclear'] for PDD in pds[:ind+1]) <= max_nuclear[ABA]
    model.maxnuclearaba=Constraint(pds, aba,rule=maxnuclearaba)

###Maximum SMR capacity (By Balancing Area)

if max_smr_aba:

    def maxsmraba(model, PD, ABA):
        ind=pds.index(PD)
        return extant_thermal[pds[0]+'.'+ABA+'.'+'nuclear_SMR']+sum(model.capacity_therm[PDD,ABA,'nuclear_SMR']-model.retire_therm[PDD,ABA,'nuclear_SMR'] for PDD in pds[:ind+1]) <= max_smr[ABA]
    model.maxsmraba=Constraint(pds, aba,rule=maxsmraba)

###Limiting CCS to Prarie Provinces (AB and SK)

def noCCSabaABSK(model, NOCCS, CCST):
    return sum(model.capacity_therm[PDD,NOCCS,CCST] for PDD in pds) <= 0
model.noCCSabaABSK=Constraint(no_CCS_aba, CCS_tech, rule=noCCSabaABSK)

###Parasitic load for CCS thermal plants

def parloadccs(model,PD,H,ABA,CCST):
    ind=pds.index(PD)
    return model.supply[PD,H,ABA,CCST]<=(sum(model.capacity_therm[PDD,ABA,CCST]-model.retire_therm[PDD,ABA,CCST] for PDD in pds[:ind+1])+extant_thermal[pds[0]+'.'+ABA+'.'+CCST])*0.8
model.parloadccs=Constraint(pds,h,aba,CCS_tech, rule=parloadccs)
if non_emitting_limit:
    #OA
    def nonemittinglimit(model,PD):
        return sum(model.supply[PD,H,ABA,NEP] for H in h for ABA in aba for NEP in non_emitting_tplants)+sum(model.windonsout[PD,H,ABA]+model.windofsout[PD,H,ABA]+model.solarout[PD,H,ABA]+ror_hydroout[PD+'.'+str(H)+'.'+ABA]+model.daystoragehydroout[PD,H,ABA]+model.monthstoragehydroout[PD,H,ABA] for H in h for ABA in aba)>=\
                                                                                                                                                                                                                                    nonemitting_limit[PD]*(sum(model.supply[PD,H,ABA,TP] for H in h for ABA in aba for TP in tplants)+sum(model.windonsout[PD,H,ABA]+model.windofsout[PD,H,ABA]+model.solarout[PD,H,ABA]+ror_hydroout[PD+'.'+str(H)+'.'+ABA]+model.daystoragehydroout[PD,H,ABA]+model.monthstoragehydroout[PD,H,ABA] for H in h for ABA in aba))
    model.nonemittinglimit=Constraint(pds, rule=nonemittinglimit)

    def nonemittinglimit2(model,PD):
        return sum(model.supply2[PD,H,ABA,NEP] for H in h for ABA in aba for NEP in non_emitting_tplants)+sum(model.windonsout2[PD,H,ABA]+model.windofsout2[PD,H,ABA]+model.solarout2[PD,H,ABA]+ror_hydroout[PD+'.'+str(H)+'.'+ABA]+model.daystoragehydroout2[PD,H,ABA]+model.monthstoragehydroout2[PD,H,ABA] for H in h for ABA in aba)>=\
                                                                                                                                                                                                                                    nonemitting_limit[PD]*(sum(model.supply2[PD,H,ABA,TP] for H in h for ABA in aba for TP in tplants)+sum(model.windonsout2[PD,H,ABA]+model.windofsout2[PD,H,ABA]+model.solarout2[PD,H,ABA]+ror_hydroout[PD+'.'+str(H)+'.'+ABA]+model.daystoragehydroout2[PD,H,ABA]+model.monthstoragehydroout2[PD,H,ABA] for H in h for ABA in aba))
    if second_dispatch_flag:
        model.nonemittinglimit2=Constraint(pds, rule=nonemittinglimit2)

if limited_new_thermal_gen:
    def limited_new_thermal(model,GT):
        return sum(model.capacity_therm[PD,ABA,GT] for PD in pds for ABA in aba)<=limited_new_thermal_gen[GT]
    model.limited_new_thermal=Constraint(list(limited_new_thermal_gen.keys()),rule=limited_new_thermal)

if renewable_portfolio_standards['wind_ons.max']:
    def rpf_wind_ons_max (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_wind_ons[PDD,GL]+ model.capacity_wind_ons_recon[PDD,GL] ) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ons'] for GL in gl if str(GL)+'.'+'wind_ons' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)])<=wind_ons_max_ap_limit[PD][AP]
    model.rpf_wind_ons_max=Constraint(pds,ap,rule=rpf_wind_ons_max)

if renewable_portfolio_standards['wind_ofs.max']:
    def rpf_wind_ofs_max (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_wind_ofs[PDD,GL]+ model.capacity_wind_ofs_recon[PDD,GL] ) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ofs'] for GL in gl if str(GL)+'.'+'wind_ofs' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)])<=wind_ofs_max_ap_limit[PD][AP]
    model.rpf_wind_ofs_max=Constraint(pds,ap,rule=rpf_wind_ofs_max)

if renewable_portfolio_standards['wind_ons.min']:
    def rpf_wind_ons_min (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_wind_ons[PDD,GL]+model.capacity_wind_ons_recon[PDD,GL]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ons'] for GL in gl if str(GL)+'.'+'wind_ons' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)])>=wind_ons_min_ap_limit[PD][AP]
    model.rpf_wind_ons_min=Constraint(pds,ap,rule=rpf_wind_ons_min)

if renewable_portfolio_standards['wind_ofs.min']:
    def rpf_wind_ofs_min (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_wind_ofs[PDD,GL]+model.capacity_wind_ofs_recon[PDD,GL]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind_ofs'] for GL in gl if str(GL)+'.'+'wind_ofs' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)])>=wind_ofs_min_ap_limit[PD][AP]
    model.rpf_wind_ofs_min=Constraint(pds,ap,rule=rpf_wind_ofs_min)

if renewable_portfolio_standards['solar.max']:
    def rpf_solar_max (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_solar[PDD,GL]+model.capacity_solar_recon[PDD,GL]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'solar'] for GL in gl if str(GL)+'.'+'solar' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)]) <=solar_max_ap_limit[PD][AP]
    model.rpf_solar_max=Constraint(pds,ap,rule=rpf_solar_max)

if renewable_portfolio_standards['solar.min']:
    def rpf_solar_min (model,PD,AP):
        ind=pds.index(PD)
        return sum((model.capacity_solar[PDD,GL]+model.capacity_solar_recon[PDD,GL]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)]) + sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'solar'] for GL in gl if str(GL)+'.'+'solar' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)]) >=solar_min_ap_limit[PD][AP]
    model.rpf_solar_min=Constraint(pds,ap,rule=rpf_solar_min)

end=time.time()

print(f'\n==================================================\n\
Creating model time: {round((end-start)/60)} Min and {round((end-start)%60)} Sec \
\n==================================================')
#option below to save LP file
#model.write('problem.lp')
start=time.time()
#solving the Optimization Problem
opt = SolverFactory('cplex')
#opt.parameters.mip.interval.set(1)
##optional settings
opt.options['mipgap'] = 0.05
#opt.options['optimality'] = 0.01
result_obj = opt.solve(model)
result_obj.write()

# Model lp
#my_path = "C:\\Users\\crist\\OneDrive\\Documentos\\UVic\\Courses\\UVIC-2021\\Fall-2021\\Co-op-Work-Term-IESVic\\IESVic-Pyomo-Test"
#my_path = "/project/rrg-mcpher16/SHARED2/copper/COPPER6_JGM"
#filename = os.path.join(my_path, 'Log-files/pyomo_model_CTE_1_5_S6.lp') # LP file of the model instance
if config_file['Simulation_Settings']['save_lp']:
    model.write('model.lp', io_options={'symbolic_solver_labels': True})

end=time.time()

print(f'\n==================================================\n\
Solving process time: {round((end-start)/60)} Min and {round((end-start)%60)} Sec\
\n==================================================')


############Saving results in .csv files################
folder_name = os.path.join(os.getcwd(), '..', 'results', scenario)
folder_name+= '_' + datetime.now().strftime(r"%Y%m%d_%H%M")

if test:
   folder_name+='_Test'
       
if not os.path.exists(folder_name):
    os.mkdir(folder_name)
os.chdir(folder_name)

ind=list(model.capacity_therm)
print('At creation of capacity therm')
print(list(model.capacity_therm))
val=list(model.capacity_therm[:,:,:].value)
print('At creation of capacity therm')
print(list(model.capacity_therm[:,:,:].value))
capacity_thermal = [ i + tuple([j]) for i,j in zip(ind, val)]
#retire_thermal = np.asarray(resultP)
np.savetxt('capacity_thermal.csv', capacity_thermal,fmt='%s',delimiter=',')

ind=list(model.retire_therm)
val=list(model.retire_therm[:,:,:].value)
retire_thermal = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('retire_thermal.csv', retire_thermal,fmt='%s',delimiter=',')

ind=list(model.capacity_wind_ofs)
val=list(model.capacity_wind_ofs[:,:].value)

capacity_wind_ofs = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_wind_ofs.csv', capacity_wind_ofs,fmt='%s',delimiter=',')

ind=list(model.capacity_wind_ons)
val=list(model.capacity_wind_ons[:,:].value)

capacity_wind_ons = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_wind_ons.csv', capacity_wind_ons,fmt='%s',delimiter=',')

ind=list(model.capacity_solar)
val=list(model.capacity_solar[:,:].value)
capacity_solar = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_solar.csv', capacity_solar,fmt='%s',delimiter=',')

ind=list(model.supply)
val=list(model.supply[:,:,:,:].value)
supply = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('supply.csv', supply,fmt='%s',delimiter=',')

ind=list(model.windonsout)
val=list(model.windonsout[:,:,:].value)
windonsout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('windonsout.csv', windonsout,fmt='%s',delimiter=',')

ind=list(model.windofsout)
val=list(model.windofsout[:,:,:].value)
windofsout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('windofsout.csv', windofsout,fmt='%s',delimiter=',')

ind=list(model.solarout)
val=list(model.solarout[:,:,:].value)
solarout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('solarout.csv', solarout,fmt='%s',delimiter=',')

ind=list(model.load_shedding)
val=list(model.load_shedding[:,:,:].value)
load_shedding = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('load_shedding.csv', load_shedding,fmt='%s',delimiter=',')

ind=list(model.storageout)
val=list(model.storageout[:,:,:,:].value)
storageout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('storageout.csv', storageout,fmt='%s',delimiter=',')

ind=list(model.storagein)
val=list(model.storagein[:,:,:,:].value)
storagein = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('storagein.csv', storagein,fmt='%s',delimiter=',')

ind=list(model.storageenergy)
val=list(model.storageenergy[:,:,:,:].value)
storageenergy = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('storageenergy.csv', storageenergy,fmt='%s',delimiter=',')

ind=list(model.daystoragehydroout)
val=list(model.daystoragehydroout[:,:,:].value)
daystoragehydroout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('daystoragehydroout.csv', daystoragehydroout,fmt='%s',delimiter=',')

ind=list(model.monthstoragehydroout)
val=list(model.monthstoragehydroout[:,:,:].value)
monthstoragehydroout = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('monthstoragehydroout.csv', monthstoragehydroout,fmt='%s',delimiter=',')

ind=list(model.transmission)
print('checking transmission end')
print(ind)
val=list(model.transmission[:,:,:,:].value)
print('value transmission')
print(val)
transmission = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('transmission.csv', transmission,fmt='%s',delimiter=',')

ind=list(model.capacity_transmission)
print('checking transmission cap end')
print(ind)
val=list(model.capacity_transmission[:,:,:].value)
print('value')
print(val)
capacity_transmission = [ i + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('capacity_transmission.csv', capacity_transmission,fmt='%s',delimiter=',')

if hydro_development:
    ind=list(model.ror_renewal_binary)
    val=list(model.ror_renewal_binary[:,:].value)
    ror_renewal_binary = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
    np.savetxt('ror_renewal_binary.csv', ror_renewal_binary,fmt='%s',delimiter=',')
   
    ind=list(model.day_renewal_binary)
    val=list(model.day_renewal_binary[:,:].value)
    day_renewal_binary = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
    np.savetxt('day_renewal_binary.csv', day_renewal_binary,fmt='%s',delimiter=',')
   
    ind=list(model.month_renewal_binary)
    val=list(model.month_renewal_binary[:,:].value)
    month_renewal_binary = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
    np.savetxt('month_renewal_binary.csv', month_renewal_binary,fmt='%s',delimiter=',')
    if not storage_continous:
        ind=list(model.pumphydro)
        val=list(model.pumphydro[:,:].value)
        pumphydro = [ tuple([i]) + tuple([j]) for i,j in zip(ind, val)]
        np.savetxt('pumphydro.csv', pumphydro,fmt='%s',delimiter=',')
   
    ind=list(model.dayrenewalout)
    val=list(model.dayrenewalout[:,:,:].value)
    dayrenewalout = [ i + tuple([j]) for i,j in zip(ind, val)]
    np.savetxt('dayrenewalout.csv', dayrenewalout,fmt='%s',delimiter=',')
   
    ind=list(model.monthrenewalout)
    val=list(model.monthrenewalout[:,:,:].value)
    monthrenewalout = [ i + tuple([j]) for i,j in zip(ind, val)]
    np.savetxt('monthrenewalout.csv', monthrenewalout,fmt='%s',delimiter=',')

if storage_continous:
    ind=list(model.capacity_storage)
    val=list(model.capacity_storage[:,:,:].value)
    capacity_storage = [ i + tuple([j]) for i,j in zip(ind, val)]
    np.savetxt('capacity_storage.csv', capacity_storage,fmt='%s',delimiter=',')
   
ind=['Objective_function_value']
val=list([model.obj()])
obj = [ tuple([i]) + tuple([j]) for i,j in zip(ind, val)]
np.savetxt('obj_value.csv', obj,fmt='%s',delimiter=',')

ind=list(model.capacity_wind_ons_recon)
val=list(model.capacity_wind_ons_recon[:,:].value)
capacity_wind_ons_recon = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_wind_ons_recon.csv', capacity_wind_ons_recon,fmt='%s',delimiter=',')

ind=list(model.capacity_wind_ofs_recon)
val=list(model.capacity_wind_ofs_recon[:,:].value)
capacity_wind_ofs_recon = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_wind_ofs_recon.csv', capacity_wind_ofs_recon,fmt='%s',delimiter=',')

ind=list(model.capacity_solar_recon)
val=list(model.capacity_solar_recon[:,:].value)
capacity_solar_recon = np.array([ tuple([i]) + tuple([j]) for i,j in zip(ind, val)], dtype=object)
np.savetxt('capacity_solar_recon.csv', capacity_solar_recon,fmt='%s',delimiter=',')

### Saving config data
number_run_days=len(rundays)
original_stdout = sys.stdout # Save a reference to the original standard output
with open('COPPER config.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print(f'Planning for the target year {foryear} considering {refyear} as the reference year')
    print(f'modeled {pds} palnning periods and ran {number_run_days} representative days in each period')
    print(f'Carbon price = {ctax}')
    print(f'pumped hydro retrofit limit = {pump_ret_limit}')
    print(f'down sampling clustering ? {downsampling}')
    print(f'hierarchical clustering ? {hierarchical}')
    print(f'test run ? {test}')
    print(f'hydro development on ? {hydro_development}')
    print(f'autarky on ? {autarky}')
    print(f'pump as continous variable ? {storage_continous}')
    print(f'provincial_emission_limit on?  {provincial_emission_limit}   {carbon_reduction} carbon reduction compared to refrence year {emission_limit_ref_year}')
    print(f'national_emission_limit on?  {national_emission_limit}  {nat_em_limit}')
    print(f'local gas price on? {local_gas_price}')
    print(f'OBPS on? {OBPS_on}')
    print(f'thermal phase out on? {thermal_phase_out} >>>>> if true these types will be phased out by the specified year {phase_out_type_year}')
    print(f'min installed gas PHP requirement on ? {min_installed_LB_PHP}')
    print(f'is developping new thermal banned ? {new_thermal_limit} >>>>> if true development of these types are banned {limited_tplants}')
    print(f'just small hydro on ? {just_small_hydro} >>>>> if true, the model just consider development of small hydro projects (under 100 MW)')
    print(f'is  technology evolution on? {technology_evolution_on}')
    print(f'is  GPS on? {GPS}')
    print(f'is  CPO on? {CPO}')
    print(f'is  tranmission expansion constrained? CTE extant = {CTE_extant} CTE coefficient =>> {CTE_coef}, CTE custom = {CTE_custom}')
    print(f'is  non_emitting limit on? {non_emitting_limit}  {nonemitting_limit}')
    print(f'limited new thermal generation expansion? {limited_new_thermal_gen} ')
    print(f'The discount rate is {discount} and the inflation is {inflation} ')
    sys.stdout = original_stdout # Reset the standard output to its original value


############### Creating the results summary excel sheet that includes generation mix, new generation capacity addition, transmission, carbon, costs, storage, renewable data
#os.chdir('C:/Users\Reza\Desktop\Google Drive\PhD\Thesis\DSF\meeting10\Final results\DSF_BAU_outputs4376_ct170_170_170_rd38_pds6_Hr_NoOBPS_LGP_NoHydro_NCL_NoPCL_CPHy_NoAr_SMR_CCS_CPO_GPS_TE')
'''
capacity_thermal=pd.read_csv(r'capacity_thermal.csv', header=None)

capacity_storage=pd.read_csv(r'capacity_storage.csv', header=None)

retire_thermal =pd.read_csv(r'retire_thermal.csv', header=None)
'''
capacity_thermal_df = pd.DataFrame(capacity_thermal, columns =['PD', 'ABA', 'Tech', 'Value'])
capacity_thermal_dict=dict(zip(list(capacity_thermal_df.iloc[:]['PD']+'.'+capacity_thermal_df.iloc[:]['ABA']+'.'+capacity_thermal_df.iloc[:]['Tech']),list(capacity_thermal_df.iloc[:]['Value'])))

retire_thermal_df = pd.DataFrame(retire_thermal, columns =['PD', 'ABA', 'Tech', 'Value'])
retire_thermal_dict=dict(zip(list(retire_thermal_df.iloc[:]['PD']+'.'+retire_thermal_df.iloc[:]['ABA']+'.'+retire_thermal_df.iloc[:]['Tech']),list(retire_thermal_df.iloc[:]['Value'])))

capacity_thermal=pd.read_csv(r'capacity_thermal.csv', header=None)

if storage_continous:
    capacity_storage=pd.read_csv(r'capacity_storage.csv', header=None)

retire_thermal =pd.read_csv(r'retire_thermal.csv', header=None)


capacity_wind_ons =pd.read_csv(r'capacity_wind_ons.csv', header=None)
capacity_wind_ons_dict=dict(zip(list(capacity_wind_ons.iloc[:][0]+capacity_wind_ons.iloc[:][1]),list(capacity_wind_ons.iloc[:][2])))
capacity_wind_ofs =pd.read_csv(r'capacity_wind_ofs.csv', header=None)
capacity_wind_ofs_dict=dict(zip(list(capacity_wind_ofs.iloc[:][0]+capacity_wind_ofs.iloc[:][1]),list(capacity_wind_ofs.iloc[:][2])))
capacity_solar = pd.read_csv(r'capacity_solar.csv', header=None)
capacity_solar_dict=dict(zip(list(capacity_solar.iloc[:][0]+capacity_solar.iloc[:][1]),list(capacity_solar.iloc[:][2])))

###Omar, double the implementation below --> supply2, windofsout2, etc.
#start here
supply =pd.read_csv(r'supply.csv', header=None)

windofsout =pd.read_csv(r'windofsout.csv', header=None)

windonsout =pd.read_csv(r'windonsout.csv', header=None)

solarout =pd.read_csv(r'solarout.csv', header=None)

load_shedding =pd.read_csv(r'load_shedding.csv', header=None)

storageout =pd.read_csv(r'storageout.csv', header=None)

storagein =pd.read_csv(r'storagein.csv', header=None)

storageenergy =pd.read_csv(r'storageenergy.csv', header=None)

monthhydroout=pd.read_csv(r'monthstoragehydroout.csv', header=None)

dayhydroout=pd.read_csv(r'daystoragehydroout.csv', header=None)

transmission = pd.read_csv(r'transmission.csv', header=None)

#ends here
capacity_transmission =pd.read_csv(r'capacity_transmission.csv', header=None)

if hydro_development:

    ror_renewal_binary =pd.read_csv(r'ror_renewal_binary.csv', header=None)
   
    day_renewal_binary =pd.read_csv(r'day_renewal_binary.csv', header=None)
   
    month_renewal_binary = pd.read_csv(r'month_renewal_binary.csv', header=None)

    dayrenewalout = pd.read_csv(r'dayrenewalout.csv', header=None)
   
    monthrenewalout = pd.read_csv(r'monthrenewalout.csv', header=None)
obj = pd.read_csv(r'obj_value.csv', header=None)

capacity_wind_ons_recon = pd.read_csv(r'capacity_wind_ons_recon.csv',header=None)
capacity_wind_ons_recon_dict=dict(zip(list(capacity_wind_ons_recon.iloc[:][0]+capacity_wind_ons_recon.iloc[:][1]),list(capacity_wind_ons_recon.iloc[:][2])))
capacity_wind_ofs_recon = pd.read_csv(r'capacity_wind_ofs_recon.csv',header=None)
capacity_wind_ofs_recon_dict=dict(zip(list(capacity_wind_ofs_recon.iloc[:][0]+capacity_wind_ofs_recon.iloc[:][1]),list(capacity_wind_ofs_recon.iloc[:][2])))
capacity_solar_recon =pd.read_csv(r'capacity_solar_recon.csv', header=None)
capacity_solar_recon_dict=dict(zip(list(capacity_solar_recon.iloc[:][0]+capacity_solar_recon.iloc[:][1]),list(capacity_solar_recon.iloc[:][2])))   
   
######### Generation outline ##########
gen_stor_types=allplants#+st
tp_num=len(tplants)
## generation mix in each period
Canada_gen_outline=np.zeros((len(pds),len(gen_stor_types)))
capcitytherm=list(capacity_thermal.iloc[:,3])
retiretherm=list(retire_thermal.iloc[:,3])
## new capacity addition in each period
Total_installed=np.zeros((len(pds),len(gen_stor_types)))
Total_retired=np.zeros((len(pds),len(tplants)))
Total_installed_hydro_aba=dict()
## provincial (BA)
ABA_generation_mix=np.zeros((len(pds)*len(aba),len(gen_stor_types)))
Total_installed_ABA=np.zeros((len(pds)*len(aba),len(gen_stor_types)))
ABA_investment_costs=np.zeros((len(pds)*len(aba),len(gen_stor_types)))
Canada_investment_costs=np.zeros((len(pds),len(gen_stor_types)))
Qualifing_capacity_summer=np.zeros((len(pds)*len(aba),len(gen_stor_types)))
ABA_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])

technologies = list(["wind_ons", "wind_ofs", "thermal", "hydro_daily", "hydro_monthly", "hydro_run", "solar"])
technologies_no_thermal = list(["wind_ons", "wind_ofs", "hydro_daily", "hydro_monthly", "hydro_run", "solar"])

margin_reserve = {AP+'.'+TECH+'.'+PD+'.'+SEAS:0 for AP in ap for TECH in technologies_no_thermal for SEAS in season for PD in pds}
margin_reserve.update({AP+'.'+TECH+'.'+PD+'.'+SEAS:0 for AP in ap for TECH in tplants for SEAS in season for PD in pds}) 

for PD in pds:
    for SEAS in season:
        for AP in ap:
            for TECH  in technologies:
                ind=pds.index(PD)
                Cap_val = 0
                if TECH in ["wind_ons", "wind_ofs"]:
                    if TECH == "wind_ons":
                        Cap_val = sum((capacity_wind_ons_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"] + capacity_wind_ons_recon_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"])*float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)])+sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'wind'] * float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]])  for GL in gl if str(GL)+'.'+'wind' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)] ) 
                        Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                    else:
                        Cap_val = sum((capacity_wind_ofs_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"] +capacity_wind_ofs_recon_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"])*float(capacity_value[SEAS]['wind'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)])
                        Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                if TECH == "thermal":
                    thermal_Cap_val = {TP:0 for TP in tplants}
                    for TP in tplants:
                        thermal_Cap_val[TP] = sum((extant_thermal[pds[0]+'.'+ABA+'.'+TP]+sum(capacity_thermal_dict[PDD+'.'+ABA+'.'+TP]-retire_thermal_dict[PDD+'.'+ABA+'.'+TP] for PDD in pds[:ind+1] )* reserve_margin_tech_dict[TP+'.'+AP] for ABA in  regions_per_province[AP] ))

                if TECH == "solar":
                    Cap_val = sum((capacity_solar_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"] + capacity_solar_recon_dict["(\'"+PDD+"\' "+"\'"+GL+"\')"])*float(capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for PDD in pds[:ind+1] for GL in gl if AP==map_gl_to_pr[int(GL)])+sum(extant_wind_solar[pds.index(PD)][str(GL)+'.'+'solar'] * float(capacity_value[SEAS]['solar'][map_gl_to_pr[int(GL)]]) for GL in gl if str(GL)+'.'+'solar' in extant_wind_solar[0] and AP==map_gl_to_pr[int(GL)] ) 
                    Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                if hydro_development:
                    if TECH == "hydro_run":
                        Cap_val = sum(capacity_ror_renewal[HR_ROR]*model.ror_renewal_binary[PDD,HR_ROR].value for PDD in pds[:ind+1] for HR_ROR in hr_ror)
                    if TECH == "hydro_daily":
                        Cap_val = sum(capacity_day_renewal[HR_DAY]*model.day_renewal_binary[PDD,HR_DAY].value for PDD in pds[:ind+1] for HR_DAY in hr_day)
                    if TECH == "hydro_monthly":
                        Cap_val = sum(capacity_month_renewal[HR_MO]*model.month_renewal_binary[PDD,HR_MO].value for PDD in pds[:ind+1] for HR_MO in hr_mo)
                
                if TECH == "hydro_run" :
                    Cap_val += sum(ror_hydro_capacity[PD+'.'+ABA] for ABA in regions_per_province[AP]) 
                    Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                if TECH == "hydro_daily":
                    Cap_val += sum(day_hydro_capacity[PD+'.'+ABA] for ABA in  regions_per_province[AP])
                    Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                if TECH == "hydro_monthly":
                    Cap_val += sum(month_hydro_capacity[PD+'.'+ABA] for ABA in  regions_per_province[AP])
                    Cap_val *= reserve_margin_tech_dict[TECH+'.'+AP]
                if TECH != "thermal":
                     margin_reserve[AP+'.'+TECH+'.'+PD+'.'+SEAS] = (Cap_val)/peak_demand[PD+'.'+SEAS+'.'+AP]
                else:
                    for TP in tplants:
                        margin_reserve[AP+'.'+TP+'.'+PD+'.'+SEAS] = (thermal_Cap_val[TP])/peak_demand[PD+'.'+SEAS+'.'+AP]

if reserve_calculation:
    with open('ABA_Tech_margin_reserve.csv', 'w') as f:
        for key in margin_reserve.keys():
            f.write("%s,%s\n"%(key,margin_reserve[key]))

Total_installed_ABA_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])
ABA_investment_costs_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])

for PD in pds:
    index_pd=pds.index(PD)
    npv_coef=sum( 1/((1+discount)**(int(PDD)-int(PD))) for PDD in pds[index_pd:])
    disc_coef_FP=(1/(1+discount)**((int(PD)-1)-2021)) #New, we are using annuity series combined with future value to present value formula
    disc_total = 0
    if index_pd != (len(pds)-1):
        #disc_ann_coef=(1-(1/(1+discount)**(int(pds[ind+1])-int(PD)))/discount)
        #We use disc_ann_coef as the coeffecient to bring an n-year series Annuity to a net present value
        first_term = (1/(1+discount)**(int(pds[-1])-int(PD))) ##We are annualizing this to the end of simulation since we are only counting things once below
        second_term = 1
        third_term = discount
        disc_ann_coef = ((second_term - first_term) / third_term)
        disc_total = disc_coef_FP*disc_ann_coef #A to P then F to P
        if (disc_ann_coef < 0):
            print(disc_ann_coef)
            sys.exit()
    else:
        disc_total=(1/(1+discount_2050)**(int(PD)-2021)) ##Use this for the final subperiod since we are not annualizing it -- treat it as a future to present value
    for ALP in gen_stor_types:
        index_p=gen_stor_types.index(ALP)
        for ABA in aba:
            index_aba=aba.index(ABA)
                           
            if ALP !='wind_ons' and ALP !='wind_ofs' and ALP!='solar' and ALP!='hydro_run' and ALP!='hydro_daily' and ALP!='hydro_monthly' and ALP not in st:
                index_tp=tplants.index(ALP)
                Canada_gen_outline[index_pd,index_p]+=extant_thermal[pds[0]+'.'+ABA+'.'+ALP]+sum(capcitytherm[ii*len(tplants)*len(aba)+index_aba*tp_num+index_tp] -retiretherm[ii*len(tplants)*len(aba)+index_aba*len(tplants)+index_tp] for ii in range(index_pd+1))
                Total_installed[index_pd,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]
                Total_retired[index_pd,index_tp]+=retiretherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]
                ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=extant_thermal[pds[0]+'.'+ABA+'.'+ALP]+sum(capcitytherm[ii*len(tplants)*len(aba)+index_aba*tp_num+index_tp] -retiretherm[ii*len(tplants)*len(aba)+index_aba*tp_num+index_tp] for ii in range(index_pd+1))
                ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': extant_thermal[pds[0]+'.'+ABA+'.'+ALP]+sum(capcitytherm[ii*len(tplants)*len(aba)+index_aba*tp_num+index_tp] -retiretherm[ii*len(tplants)*len(aba)+index_aba*tp_num+index_tp] for ii in range(index_pd+1))}, index=[0])])
                Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]
                Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                if flag_itc:
                    if ALP in ITCplants:
                        if PD in ['2025', '2030']:
                            Canada_investment_costs[index_pd,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]* factor_itc *capitalcost[ABA+'.'+ALP]*(disc_total)
                            ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]* factor_itc *capitalcost[ABA+'.'+ALP]*(disc_total)
                            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        elif PD in ['2035', '2040', '2045', '2050']:
                            Canada_investment_costs[index_pd,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                            ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                    else:
                        Canada_investment_costs[index_pd,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                else:
                    Canada_investment_costs[index_pd,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                    ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capcitytherm[index_pd*len(tplants)*len(aba)+index_aba*tp_num+index_tp]*capitalcost[ABA+'.'+ALP]*(disc_total)
                    ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
            elif ALP =='hydro_run' or ALP =='hydro_daily' or ALP =='hydro_monthly':
                if ALP =='hydro_run':
                    Canada_gen_outline[index_pd,index_p]+=ror_hydro_capacity[PD+'.'+ABA]
                    ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=ror_hydro_capacity[PD+'.'+ABA]
                elif ALP =='hydro_daily':
                    Canada_gen_outline[index_pd,index_p]+=day_hydro_capacity[PD+'.'+ABA]
                    ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=day_hydro_capacity[PD+'.'+ABA]
                elif ALP =='hydro_monthly':
                    Canada_gen_outline[index_pd,index_p]+=month_hydro_capacity[PD+'.'+ABA]
                    ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=month_hydro_capacity[PD+'.'+ABA]
                cap = ror_hydro_capacity[PD+'.'+ABA]+day_hydro_capacity[PD+'.'+ABA]+month_hydro_capacity[PD+'.'+ABA]
                ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                if hydro_development:
                    if ALP =='hydro_run':
                        Total_installed_hydro_aba[PD+'.'+ABA+'.ror']=0
                    elif ALP =='hydro_daily':
                        Total_installed_hydro_aba[PD+'.'+ABA+'.day']=0
                    elif ALP =='hydro_monthly':
                        Total_installed_hydro_aba[PD+'.'+ABA+'.month']=0
                    '''
                    Total_installed_hydro_aba[PD+'.'+ABA+'.ror']=0
                    Total_installed_hydro_aba[PD+'.'+ABA+'.day']=0
                    Total_installed_hydro_aba[PD+'.'+ABA+'.month']=0
                    '''
 
                    ##The code below must be wrong, this would be repeated 3 times by default
                    if ALP =='hydro_run':
                        for HR_ROR in hr_ror:
                            if ABA==location_renewal[HR_ROR]:
                                index_rn=hr_ror.index(HR_ROR)
                                Canada_gen_outline[index_pd,index_p]+=sum(ror_renewal_binary.iloc[index_rn+ii*len(hr_ror)][2]*capacity_renewal[HR_ROR] for ii in range(index_pd+1))
                                Total_installed[index_pd,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*capacity_renewal[HR_ROR]
                                Total_installed_hydro_aba[PD+'.'+ABA+'.ror']+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*capacity_renewal[HR_ROR]
                                ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(ror_renewal_binary.iloc[index_rn+ii*len(hr_ror)][2]*capacity_renewal[HR_ROR] for ii in range(index_pd+1))
                                cap = sum(ror_renewal_binary.iloc[index_rn+ii*len(hr_ror)][2]*capacity_renewal[HR_ROR] for ii in range(index_pd+1))
                                ABA_df = pd.concat([ABA_df,pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                                Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*capacity_renewal[HR_ROR]
                                Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                if flag_itc:
                                    if PD in ['2025', '2030']:
                                        Canada_investment_costs[index_pd,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]* factor_itc *cost_ror_renewal[HR_ROR]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]* factor_itc *cost_ror_renewal[HR_ROR]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                    elif PD in ['2035', '2040', '2045', '2050']:
                                        Canada_investment_costs[index_pd,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*cost_ror_renewal[HR_ROR]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*cost_ror_renewal[HR_ROR]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                else:
                                    Canada_investment_costs[index_pd,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*cost_ror_renewal[HR_ROR]*(disc_total)
                                    ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=ror_renewal_binary.iloc[index_rn+index_pd*len(hr_ror)][2]*cost_ror_renewal[HR_ROR]*(disc_total)
                                    ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])

                    if ALP =='hydro_daily':
                        for HR_DAY in hr_day:
                            if ABA==location_renewal[HR_DAY]:
                                index_rn=hr_day.index(HR_DAY)
                                Canada_gen_outline[index_pd,index_p]+=sum(day_renewal_binary.iloc[index_rn+ii*len(hr_day)][2]*capacity_renewal[HR_DAY] for ii in range(index_pd+1))
                                Total_installed[index_pd,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*capacity_renewal[HR_DAY]
                                Total_installed_hydro_aba[PD+'.'+ABA+'.day']+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*capacity_renewal[HR_DAY]
                                ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(day_renewal_binary.iloc[index_rn+ii*len(hr_day)][2]*capacity_renewal[HR_DAY] for ii in range(index_pd+1))
                                cap = sum(day_renewal_binary.iloc[index_rn+ii*len(hr_day)][2]*capacity_renewal[HR_DAY] for ii in range(index_pd+1))
                                ABA_df = pd.concat([ABA_df,pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                                Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*capacity_renewal[HR_DAY]
                                Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                if flag_itc:
                                    if PD in ['2025', '2030']:
                                        Canada_investment_costs[index_pd,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]* factor_itc *cost_day_renewal[HR_DAY]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]* factor_itc *cost_day_renewal[HR_DAY]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                    elif PD in ['2035', '2040', '2045', '2050']:
                                        Canada_investment_costs[index_pd,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*cost_day_renewal[HR_DAY]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*cost_day_renewal[HR_DAY]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                else:
                                    Canada_investment_costs[index_pd,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*cost_day_renewal[HR_DAY]*(disc_total)
                                    ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=day_renewal_binary.iloc[index_rn+index_pd*len(hr_day)][2]*cost_day_renewal[HR_DAY]*(disc_total)
                                    ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
    
                    if ALP =='hydro_monthly':
                        for HR_MO in hr_mo:
                            if ABA==location_renewal[HR_MO]:
                                index_rn=hr_mo.index(HR_MO)
                                Canada_gen_outline[index_pd,index_p]+=sum(month_renewal_binary.iloc[index_rn+ii*len(hr_mo)][2]*capacity_renewal[HR_MO] for ii in range(index_pd+1))
                                Total_installed[index_pd,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*capacity_renewal[HR_MO]
                                Total_installed_hydro_aba[PD+'.'+ABA+'.month']+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*capacity_renewal[HR_MO]
                                ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(month_renewal_binary.iloc[index_rn+ii*len(hr_mo)][2]*capacity_renewal[HR_MO] for ii in range(index_pd+1))
                                cap = sum(month_renewal_binary.iloc[index_rn+ii*len(hr_mo)][2]*capacity_renewal[HR_MO] for ii in range(index_pd+1))
                                ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                                Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*capacity_renewal[HR_MO]
                                Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                if flag_itc:
                                    if PD in ['2025', '2030']:
                                        Canada_investment_costs[index_pd,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]* factor_itc *cost_month_renewal[HR_MO]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]* factor_itc *cost_month_renewal[HR_MO]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                    elif PD in ['2035', '2040', '2045', '2050']:
                                        Canada_investment_costs[index_pd,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*cost_month_renewal[HR_MO]*(disc_total)
                                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*cost_month_renewal[HR_MO]*(disc_total)
                                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                                else:
                                    Canada_investment_costs[index_pd,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*cost_month_renewal[HR_MO]*(disc_total)
                                    ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=month_renewal_binary.iloc[index_rn+index_pd*len(hr_mo)][2]*cost_month_renewal[HR_MO]*(disc_total)
                                    ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
            elif ALP == 'wind_ofs' or ALP == 'wind_ons' or ALP == 'solar':
                   
                for GL in gl:
                    if map_gl_to_ba[int(GL)]==ABA and str(GL)+'.'+ALP in extant_wind_solar[0]:
                        Canada_gen_outline[index_pd,index_p]+=extant_wind_solar[pds.index(PD)][str(GL)+'.'+ALP]
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=extant_wind_solar[pds.index(PD)][str(GL)+'.'+ALP]  
                        cap = extant_wind_solar[pds.index(PD)][str(GL)+'.'+ALP]
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                    if map_gl_to_ba[int(GL)]==ABA and ALP=='wind_ons':
                        Canada_gen_outline[index_pd,index_p]+=sum(capacity_wind_ons.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ons_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        #Canada_gen_outline[index_pd,index_p]+=sum(capacity_wind.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))+capacity_wind_recon.iloc[len(gl)*0+int(GL)-1][2]
                        Total_installed[index_pd,index_p]+=capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(capacity_wind_ons.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ons_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        cap = sum(capacity_wind_ons.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ons_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                        Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        if flag_itc:
                            if PD in ['2025', '2030']:
                                Canada_investment_costs[index_pd,index_p]+=capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                            elif PD in ['2035', '2040', '2045', '2050']:
                                Canada_investment_costs[index_pd,index_p]+=capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        else:
                            Canada_investment_costs[index_pd,index_p]+=capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                            ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ons.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ons_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                    if map_gl_to_ba[int(GL)]==ABA and ALP=='wind_ofs':
                        Canada_gen_outline[index_pd,index_p]+=sum(capacity_wind_ofs.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ofs_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        #Canada_gen_outline[index_pd,index_p]+=sum(capacity_wind.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))+capacity_wind_recon.iloc[len(gl)*0+int(GL)-1][2]
                        Total_installed[index_pd,index_p]+=capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(capacity_wind_ofs.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ofs_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        cap = sum(capacity_wind_ofs.iloc[len(gl)*ii+int(GL)-1][2]+capacity_wind_ofs_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                        Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        if flag_itc:
                            if PD in ['2025', '2030']:
                                Canada_investment_costs[index_pd,index_p]+=capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                            elif PD in ['2035', '2040', '2045', '2050']:
                                Canada_investment_costs[index_pd,index_p]+=capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        else:
                            Canada_investment_costs[index_pd,index_p]+=capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                            ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_wind_ofs.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost[PD+'.'+GL]*disc_total+capacity_wind_ofs_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*windonscost_recon[PD]*disc_total
                            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])

                    if map_gl_to_ba[int(GL)]==ABA and ALP=='solar':
                        Canada_gen_outline[index_pd,index_p]+=sum(capacity_solar.iloc[len(gl)*ii+int(GL)-1][2]+capacity_solar_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        #Canada_gen_outline[index_pd,index_p]+=sum(capacity_solar.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))+capacity_solar_recon.iloc[len(gl)*0+int(GL)-1][2]
                        Total_installed[index_pd,index_p]+=capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=sum(capacity_solar.iloc[len(gl)*ii+int(GL)-1][2]+capacity_solar_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        cap = sum(capacity_solar.iloc[len(gl)*ii+int(GL)-1][2]+capacity_solar_recon.iloc[len(gl)*ii+int(GL)-1][2] for ii in range(index_pd+1))
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                        Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]
                        Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        if flag_itc:
                            if PD in ['2025', '2030']:
                                Canada_investment_costs[index_pd,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]* factor_itc *solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                            elif PD in ['2035', '2040', '2045', '2050']:
                                Canada_investment_costs[index_pd,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                                ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                                ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                        else:
                            Canada_investment_costs[index_pd,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                            ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+= capacity_solar.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost[PD+'.'+GL]*disc_total+capacity_solar_recon.iloc[len(gl)*index_pd+int(GL)-1][2]*solarcost_recon[PD]*disc_total
                            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
            elif ALP in st:
                if storage_continous:
                    if ST=='storage_LI' and int(PD)>=2040:
                        Canada_gen_outline[index_pd,index_p]+=ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd-2,index_pd+1))
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd-2,index_pd+1))
                        cap = ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd-2,index_pd+1))
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                    else:
                        Canada_gen_outline[index_pd,index_p]+=ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd+1))
                        ABA_generation_mix[len(aba)*index_pd+index_aba,index_p]+=ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd+1))
                        cap = ba_storage_capacity[ABA+'.'+ALP]+sum(capacity_storage.iloc[ii*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3] for ii in range(index_pd+1))
                        ABA_df = pd.concat([ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': cap}, index=[0])])
                Total_installed[index_pd,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]
                Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]
                Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': Total_installed_ABA[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                if flag_itc:
                    if PD in ['2025', '2030']:
                        Canada_investment_costs[index_pd,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]* factor_itc *storage_cost[ST+'.'+PD]*disc_total
                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]* factor_itc *storage_cost[ST+'.'+PD]*disc_total
                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                    elif PD in ['2035', '2040', '2045', '2050']:
                        Canada_investment_costs[index_pd,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]*storage_cost[ST+'.'+PD]*disc_total
                        ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]*storage_cost[ST+'.'+PD]*disc_total
                        ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])
                else:
                    Canada_investment_costs[index_pd,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]*storage_cost[ST+'.'+PD]*disc_total
                    ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]+=capacity_storage.iloc[index_pd*len(st)*len(aba)+st.index(ALP)*len(aba)+index_aba][3]*storage_cost[ST+'.'+PD]*disc_total
                    ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': PD, 'balancing_area': ABA, 'generation_type': ALP, 'capacity': ABA_investment_costs[len(aba)*index_pd+index_aba,index_p]}, index=[0])])

ABA_df = ABA_df.applymap(lambda s: s.lower() if isinstance(s, str) else s)
ABA_df = ABA_df.groupby(['year','balancing_area','generation_type']).sum()
#ABA_df.to_csv('ABA_generation_mix.csv',index=False)
new_ABA_generation_mix_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])
Total_installed_ABA_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])
ABA_investment_costs_df = pd.DataFrame(columns=['year', 'balancing_area', 'generation_type', 'capacity'])
for PD_index in range(len(pds)):
    for ABA_index in range(len(aba)):
        for generation_type_index in range(len(gen_stor_types)):
            new_ABA_generation_mix_df = pd.concat([new_ABA_generation_mix_df, pd.DataFrame({'year': pds[PD_index], 'balancing_area': aba[ABA_index], 'generation_type': gen_stor_types[generation_type_index], 'capacity': ABA_generation_mix[len(aba)*PD_index+ABA_index,generation_type_index]}, index=[0])]) 
            Total_installed_ABA_df = pd.concat([Total_installed_ABA_df, pd.DataFrame({'year': pds[PD_index], 'balancing_area': aba[ABA_index], 'generation_type': gen_stor_types[generation_type_index], 'capacity': Total_installed_ABA[len(aba)*PD_index+ABA_index,generation_type_index]}, index=[0])])
            ABA_investment_costs_df = pd.concat([ABA_investment_costs_df, pd.DataFrame({'year': pds[PD_index], 'balancing_area': aba[ABA_index], 'generation_type': gen_stor_types[generation_type_index], 'capacity': ABA_investment_costs[len(aba)*PD_index+ABA_index,generation_type_index]}, index=[0])])


new_ABA_generation_mix_df = new_ABA_generation_mix_df.applymap(lambda s: s.lower() if isinstance(s, str) else s)
#
new_ABA_generation_mix_df = new_ABA_generation_mix_df.groupby(['year','generation_type','balancing_area'], group_keys=False).sum().reset_index()
#
new_ABA_generation_mix_df.to_csv('new_ABA_generation_mix.csv',index=False)


provinces_per_region = {k.lower(): v for k, v in provinces_per_region.items()}
qualifing_capacity_summer = {k.lower(): v for k, v in qualifing_capacity_summer.items()}
qualifing_capacity_winter = {k.lower(): v for k, v in qualifing_capacity_winter.items()}

Qualifying_capacity_summer_df = new_ABA_generation_mix_df.copy()
Qualifying_capacity_winter_df = new_ABA_generation_mix_df.copy()
for index in range(len(Qualifying_capacity_winter_df.values)):
    row = Qualifying_capacity_winter_df.values[index]
    if row[1] == 'solar':
        Qualifying_capacity_winter_df.at[index,'capacity'] *= qualifing_capacity_winter['solar.winter.'+provinces_per_region[row[2]][0].lower()+'.a']
    elif row[1] == 'wind_ons':
        Qualifying_capacity_winter_df.at[index,'capacity'] *= qualifing_capacity_winter['wind.winter.'+provinces_per_region[row[2]][0].lower()+'.a']
    elif row[1] == 'wind_ofs':
        Qualifying_capacity_winter_df.at[index,'capacity'] *= qualifing_capacity_winter['wind.winter.'+provinces_per_region[row[2]][0].lower()+'.a']
    else:
        Qualifying_capacity_winter_df.at[index,'capacity'] *= qualifing_capacity_winter[row[1]+'.'+provinces_per_region[row[2]][0].lower()]

for index in range(len(Qualifying_capacity_summer_df.values)):
    row = Qualifying_capacity_summer_df.values[index]
    if row[1] == 'solar':
        Qualifying_capacity_summer_df.at[index,'capacity'] *= qualifing_capacity_summer['solar.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    if row[1] == 'wind_ons':
        Qualifying_capacity_summer_df.at[index,'capacity'] *= qualifing_capacity_summer['wind.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    if row[1] == 'wind_ofs':
        Qualifying_capacity_summer_df.at[index,'capacity'] *= qualifing_capacity_summer['wind.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    else:
        Qualifying_capacity_summer_df.at[index,'capacity'] *= qualifing_capacity_summer[row[1]+'.'+provinces_per_region[row[2]][0].lower()]


for index in range(len(Qualifying_capacity_summer_df.values)):
    row = Qualifying_capacity_summer_df.values[index]
    if row[1] == 'solar':
        Qualifying_capacity_summer_df.values[index][3] *= qualifing_capacity_summer['solar.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    if row[1] == 'wind_ons':
        Qualifying_capacity_summer_df.values[index][3] *= qualifing_capacity_summer['wind.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    if row[1] == 'wind_ofs':
        Qualifying_capacity_summer_df.values[index][3] *= qualifing_capacity_summer['wind.summer.'+provinces_per_region[row[2]][0].lower()+'.a']
    else:
        Qualifying_capacity_summer_df.values[index][3] *= qualifing_capacity_summer[row[1]+'.'+provinces_per_region[row[2]][0].lower()]



Qualifying_capacity_summer_df.to_csv('Qualifying_capacity_summer.csv',index=False)
Qualifying_capacity_winter_df.to_csv('Qualifying_capacity_winter.csv',index=False)


Total_installed_ABA_df = Total_installed_ABA_df.applymap(lambda s: s.lower() if isinstance(s, str) else s)
Total_installed_ABA_df = Total_installed_ABA_df.groupby(['year','generation_type','balancing_area'], group_keys=False).sum().reset_index()
Total_installed_ABA_df.to_csv('Total_installed_ABA.csv',index=False)

ABA_investment_costs_df = ABA_investment_costs_df.applymap(lambda s: s.lower() if isinstance(s, str) else s)
ABA_investment_costs_df = ABA_investment_costs_df.groupby(['year','generation_type','balancing_area'], group_keys=False).sum().reset_index()
ABA_investment_costs_df.to_csv('ABA_investment_costs.csv',index=False)
##### installed transmission ##########3
Installed_transmission=np.zeros((len(pds),len(transmap)))
Transmission_investment=np.zeros((len(pds),len(transmap)))
tr_list=list(capacity_transmission.iloc[:][1])
iter_index=-1  
for PD in pds:
    index_pd=pds.index(PD)
    npv_coef=sum( 1/((1+discount)**(int(PDD)-int(PD))) for PDD in pds[index_pd:])
    for ABA in aba:
        for ABBA in aba:
            if ABA+'.'+ABBA in transmap:
                Installed_transmission[pds.index(PD),transmap.index(ABA+'.'+ABBA)]=capacity_transmission.iloc[pds.index(PD)*len(aba)*len(aba)+aba.index(ABA)*len(aba)+aba.index(ABBA)][3]
                Transmission_investment[pds.index(PD),transmap.index(ABA+'.'+ABBA)]=capacity_transmission.iloc[pds.index(PD)*len(aba)*len(aba)+aba.index(ABA)*len(aba)+aba.index(ABBA)][3]*transcost*distance[ABA+'.'+ABBA]*npv_coef
       
###### Calculating Carbon Emission by BA ######
#start here repeat for supply2
carbon_ABA=np.zeros((len(pds),len(aba)))
carbon_national_tp=np.zeros((len(pds),len(tplants)))
hours_list=list(supply.iloc[:][0])
ba_list=list(supply.iloc[:][1])
tp_type=list(supply.iloc[:][2])
prod_power=list(supply.iloc[:][3])
carbon_national=dict()
carbon_ABA_Tech={PD+'.'+ABA+'.'+TP:0 for PD in pds for ABA in aba for TP in tplants}
carbon_AP_Tech={PD+'.'+AP+'.'+TP:0 for PD in pds for AP in ap for TP in tplants}
for PD in pds:
    carbon_national[PD]=0
    for H in h:
        for ABA in aba:
            for TP in tplants:
                current_hour_carbon_emession=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] *carbondioxide[TP]*365/len(run_days)/1000000
                carbon_ABA_Tech[PD+'.'+ABA+'.'+TP]+=current_hour_carbon_emession
                carbon_ABA[pds.index(PD),aba.index(ABA)]+=current_hour_carbon_emession
                carbon_national[PD]+=current_hour_carbon_emession
                carbon_national_tp[pds.index(PD),tplants.index(TP)]+=current_hour_carbon_emession
carbon_ap=np.zeros((len(pds),len(ap)))
for AP in ap:
        for PD in pds:
            for ABA in aba:
                if AP in ABA:
                    carbon_ap[pds.index(PD),ap.index(AP)]+=carbon_ABA[pds.index(PD),aba.index(ABA)]
                    for TP in tplants:
                        carbon_AP_Tech[PD+'.'+AP+'.'+TP]+=carbon_ABA_Tech[PD+'.'+ABA+'.'+TP]


carbon_cost_ABA=np.zeros((len(pds),len(aba),len(tplants)))
for PD in pds:
    for H in h:
        for ABA in aba:
            for TP in tplants:
                if PD == '2025': ## In 2025 the OBPS is in effect no matter what
                    carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2025[TP] * ctax[PD]
                elif PD == '2030': ## In 2030 the OBPS is in effect no matter what
                    carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2030[TP] * ctax[PD]
                elif flag_cer: ## If CER is on, then full carbon tax is applied 2035-2050
                    carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_cer[TP] * ctax[PD]
                else: # If CER is false, then OBPS carbon tax is applied 2035-2050 with the OBPS 2030 rate
                    carbon_cost_ABA[pds.index(PD),aba.index(ABA), tplants.index(TP)]+=supply.iloc[pds.index(PD)*len(h)*len(aba)*len(tplants)+h.index(H)*len(aba)*len(tplants)+aba.index(ABA)*len(tplants)+tplants.index(TP)][4] * tplant_fuel_co2_dict_obps_2030[TP] * ctax[PD]

carbon_cost_ap=np.zeros((len(pds),len(ap),len(tplants)))
for AP in ap:
    for PD in pds:
        for ABA in aba:
            for TP in tplants:
                if AP in ABA:
                    carbon_cost_ap[pds.index(PD),ap.index(AP),tplants.index(TP)]+=carbon_cost_ABA[pds.index(PD),aba.index(ABA),tplants.index(TP)]


#end
#Obj=obj.iloc[0][1]*365/len(run_days)/1000000  -The objective value was already scaled to a year, no need to apply cap cost alteration here 
Obj=obj.iloc[0][1]/1000000     

##Repeat the above operations Omar -JGM

#generation by source TWH

j=0
Canada_supply_source=np.zeros((len(pds),len(allplants)))
for i in list(supply[0]):
    pds_index=pds.index(str(i))
    allplants_index=allplants.index(supply[3][j])
    Canada_supply_source[pds_index,allplants_index]+=supply[4][j]
    j+=1

'''
iterating through each hour in the set of rundays, increment the Canada_supply_source with a value from each source (wind_ons, wind_ofs, solar and hydro) for the current hour.
'''
j=0
for i in list(windonsout[0]):
    pds_index=pds.index(str(i))
    allplants_index=allplants.index('wind_ons')
    Canada_supply_source[pds_index,allplants_index]+=windonsout[3][j]
    allplants_index=allplants.index('wind_ofs')
    Canada_supply_source[pds_index,allplants_index]+=windofsout[3][j]
    allplants_index=allplants.index('solar')
    Canada_supply_source[pds_index,allplants_index]+=solarout[3][j]
    allplants_index=allplants.index('hydro_run')
    Canada_supply_source[pds_index,allplants_index]+=monthhydroout[3][j]
    Canada_supply_source[pds_index,allplants_index]+=dayhydroout[3][j]
    Canada_supply_source[pds_index,allplants_index]+=ror_hydroout[str(i)+'.'+str(windonsout[1][j])+'.'+windonsout[2][j]]
    j+=1

    #current_key = str(i)+'.'+str(windonsout[1][j])+'.'+windonsout[2][j]
    #current_value = ror_hydroout[str(i)+'.'+str(windonsout[1][j])+'.'+windonsout[2][j]]

Canada_supply_source=Canada_supply_source.transpose()
carbon_ap=carbon_ap.transpose()
carbon_ABA=carbon_ABA.transpose()
carbon_cost_ap = carbon_cost_ap.transpose()
carbon_national_tp=carbon_national_tp.transpose()                            
Canada_gen_outline=Canada_gen_outline.transpose()
Total_installed=Total_installed.transpose()
ABA_generation_mix=ABA_generation_mix.transpose()
Total_installed_ABA=Total_installed_ABA.transpose()  
Installed_transmission=Installed_transmission.transpose()
ABA_investment_costs=ABA_investment_costs.transpose()
Transmission_investment=Transmission_investment.transpose()  
Canada_investment_costs=Canada_investment_costs.transpose()

'''
carbon_cost_ap_df= pd.DataFrame(carbon_cost_ap, columns = None, index=gen_stor_types)
carbon_cost_ap_df.to_excel("Results_summary.xlsx",sheet_name='carbon_cost_ap')
'''
can_gen_outline_df= pd.DataFrame(Canada_gen_outline, columns = pds, index=gen_stor_types)
can_gen_outline_df.to_excel("Results_summary.xlsx",sheet_name='Canada_generation_mix')

Canada_supply_source_df= pd.DataFrame(Canada_supply_source,index=allplants,columns = pds)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Canada_supply_source_df.to_excel(writer, sheet_name='supply_by_source')

Total_installed_df= pd.DataFrame(Total_installed, columns = pds, index=gen_stor_types)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Total_installed_df.to_excel(writer, sheet_name='New_installed_capacity')

with pd.ExcelWriter('Results_summary.xlsx',mode='a',engine="openpyxl") as writer: 
    new_ABA_generation_mix_df.to_excel(writer, sheet_name='ABA_generation_mix', index=False)

with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Total_installed_ABA_df.to_excel(writer, sheet_name='New_installed_ABA')
   
   
Installed_transmission_df= pd.DataFrame(Installed_transmission, index=transmap,columns = pds)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Installed_transmission_df.to_excel(writer, sheet_name='New_installed_transmission')


''' new carbon emessioons by province and technology'''
carbon_ap_tech_df = pd.DataFrame([(k, v) for k, v in carbon_AP_Tech.items()], columns=['Generation Type per Province per Period', 'value'])
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    carbon_ap_tech_df.to_excel(writer, sheet_name='carbon_AP_Tech')


carbon_national_df= pd.DataFrame(list(carbon_national.values()), index=pds,columns=['MT CO2'])
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    carbon_national_df.to_excel(writer, sheet_name='carbon_national')


carbon_ap_df= pd.DataFrame(carbon_ap, index=ap,columns = pds)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    carbon_ap_df.to_excel(writer, sheet_name='carbon_AP')

carbon_ABA_df= pd.DataFrame(carbon_ABA, index=aba,columns = pds)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    carbon_ABA_df.to_excel(writer, sheet_name='carbon_ABA')

carbon_national_tp_df= pd.DataFrame(carbon_national_tp, index=tplants,columns = pds)
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    carbon_national_tp_df.to_excel(writer, sheet_name='carbon_national_tp')

Canada_investment_costs_df= pd.DataFrame(Canada_investment_costs, index=gen_stor_types,columns = pds)    
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Canada_investment_costs_df.to_excel(writer, sheet_name='Canada_investment_costs')
       
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    ABA_investment_costs_df.to_excel(writer, sheet_name='ABA_investment_costs')


Transmission_investment_df= pd.DataFrame(Transmission_investment, index=transmap,columns = pds)    
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    Transmission_investment_df.to_excel(writer, sheet_name='Transmission_investment')

obj_df= pd.DataFrame([Obj],index=['Total cost'],columns = ['M$'])    
with pd.ExcelWriter('Results_summary.xlsx',
                    mode='a',engine="openpyxl") as writer:  
    obj_df.to_excel(writer, sheet_name='OBJ')

with pd.ExcelWriter('Results_summary.xlsx',mode='a',engine="openpyxl") as writer: 
    Qualifying_capacity_summer_df.to_excel(writer, sheet_name='Qualifying_capacity_summer', index=False)
   
with pd.ExcelWriter('Results_summary.xlsx',mode='a',engine="openpyxl") as writer: 
    Qualifying_capacity_winter_df.to_excel(writer, sheet_name='Qualifying_capacity_winter', index=False)
   
### Creates a report when the model is infeasible
original_stdout = sys.stdout # Save a reference to the original standard output
with open('solver_report.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.    
    #model.load(result_obj) # Loading solution into results object    
    if (result_obj.solver.status == SolverStatus.ok) and (result_obj.solver.termination_condition == TerminationCondition.optimal):
        print('Terminated optimal and normal\n\n')
    elif (result_obj.solver.termination_condition == TerminationCondition.infeasible):
        print('WARNING:    -------- INFEASIBLE :(  -------\n\n')
        #from pyomo.util.infeasible import log_infeasible_constraints
        #import logging
        #log_infeasible_constraints(model)
        # log_infeasible_constraints(model, log_expression=True, log_variables=True)
        #logging.basicConfig(filename='infeasible.log', level=logging.INFO)
    else:
        # Something else is wrong
        print('Solver Status:',  result_obj.solver.status,'\n\n')
    result_obj.write()
    sys.stdout = original_stdout # Reset the standard output to its original value