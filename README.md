# COPPER 9.0
## Description
The **C**anadian **O**pportunities for **P**lanning and **P**roduction of **E**lectricity **R**esources (COPPER) framework is an electricity system planning model. It minimizes total system costs (including investment, operation and maintenance costs) over an extended planning period. The COPPER framework builds upon the CREST framework developed by Dolter and Rivers (Dolter and Rivers, 2018) with several important modifications. 

Updated mathematical notation for COPPER 9.0 is availaible in the documentation folder

### Authors:

- Reza Arjmand
- Dr. Madeleine McPherson
- Dr. Jacob G. Monroe
- Keegan Griffiths
- Omar Attia
- Nathan de Matos
- Mackenzie Judson
- Cristiano Fernandes
- David Huckerbrink
- Ahnaf Ahmed
- Dominic Rivest

## Quickstart

1) Clone the repository:
    * `git clone https://gitlab.com/McPherson/copper.git`

2) Install either [Anaconda](https://www.anaconda.com/) or [Miniconda](https://docs.conda.io/projects/miniconda/en/latest/). During installation make sure you add `conda` to your path variable.

3) Open your local version of copper (the directory, cloned in step 1) and enter the following commands to create and activate the conda environment:
    * `conda env create -f env.yml`
    * `conda activate copper` (You have to activate the environment each time the promt is started.)
 
4) Run COPPER
    * `cd COPPER8`
    * `python COPPER8.0.py`


## Release Notes

### Version 7.2 Nov 2021

- Converted windg solarg from equality to less than equal constraints
- Done: technology evolution, limit new thermal generation, thermal phase out, just small hydro, national carbon limit
- Done: new storage technology, fix pumped continues to include new build
- Done: limited new capacity of a specific generation type
- Done: Solved tranmission  cap problem
- Done: Transmission expansion limit
- New: supply by source
- New: added a provincial breakdown of new installed capacity
- New: added supply by source
- discount
- renewable min max limit per AP

### Version 7.3
New: added cost breakdown

### Version 8.0 Jun 2023
New:
- wind offshore
- CODERS 2021 data

### Version 9.0 October 2023
New:
- CER constraints (w/ new generation data)
- Better OBPS implementation
- Load Shedding constraint
- Better handling of inputs
- Faster computing time for inputs
- Better handling of hydro renewal binaries
- demand.csv and us_demand.csv synchronized for UTC
- fix of transcap constraint 

## References
Dolter, B., Rivers, N., 2018. The cost of decarbonizing the Canadian electricity system. Energy Policy 113, 135–148. https://doi.org/10.1016/j.enpol.2017.10.040

COPPER has been written by Reza Arjmand, Ph.D candidate at UVic (rezaarjmand@uvic.ca; rezaarjmand@yahoo.com). Linkedin: www.linkedin.com/in/rezaarjmand.

Please cite the paper below:

Arjmand, Reza, and Madeleine McPherson. "Canada's electricity system transition under alternative policy scenarios." Energy Policy 163 (2022): 112844. https://doi.org/10.1016/j.enpol.2022.112844
